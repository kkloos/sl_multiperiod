from framework.utils.data_interface import Pickle
from framework.ex_post import run_expost
from framework.percommit import run_per_commit
from framework.structure import Setup
from framework.heuristic import run_heuristic
from framework.lp_approx import run_lp_approx
import pandas as pd
import framework.analyze as alz
import logging

#logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.ERROR)

logger = logging.getLogger()
logger.disabled = False
logger.level = logging.WARN
p = Pickle(2)

setup = Setup.from_dict(p['setup'])
demands = p['demands']
allocations = dict()
pd_results = pd.DataFrame()


# logging.info("Start Heuristic")
# df, allocations['heuristic-mma'] = run_heuristic(setup, demands, mode='mma')
# pd_results = pd_results.append(df)
#
# logging.info("Start Heuristic")
# df, allocations['heuristic-ccsaq'] = run_heuristic(setup, demands, mode='ccsaq')
# pd_results = pd_results.append(df)
#
# logging.info("Start Heuristic")
# df, allocations['heuristic-nlopt'] = run_heuristic(setup, demands, mode='nlopt')
# pd_results = pd_results.append(df)

logging.info("Start Heuristic")
df, allocations['heuristic-nlopt-ccsaq'] = run_heuristic(setup, demands, mode='nlopt-ccsaq')
pd_results = pd_results.append(df)

#logging.info("Start Heuristic")
#df, allocations['heuristic-cobyla'] = run_heuristic(setup, demands, mode='cobyla')
#pd_results = pd_results.append(df)
#

logging.info("Start LP-Approximation")
df, allocations['lpapprox'] = run_lp_approx(setup, demands)
pd_results = pd_results.append(df)

logging.info("Start Expost")
df, allocations['expost'] = run_expost(setup, demands)
pd_results = pd_results.append(df)

logging.info("Start Per Commit")
df, allocations['percommit'] = run_per_commit(setup, demands)
pd_results = pd_results.append(df)

p['allocations'] = allocations
p['results'] = pd_results

p.save()
