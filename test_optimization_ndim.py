# -*- coding: utf-8 -*-
"""
Created on Wed Aug 02 08:27:26 2017

@author: kok29wv
"""

import framework.objects as opt
import numpy as np
# import matplotlib.pyplot as plt
# import seaborn as sns
import framework.period
import pandas as pd
import time

# define customers


customers = []
customers.append(opt.Customer(0.9, 1000, 10, 2))
customers.append(opt.Customer(0.9, 5000, 10, 2))
customers.append(opt.Customer(0.95, 1000, 10, 2))

sl_a = np.linspace(0.85, 1, 20)
costs = list()
allocations = list()
results = pd.DataFrame()
speed = pd.DataFrame()
lperiod = framework.period.LastPeriod(customers, 30)
lperiod.slsqp = True
start = time.time()
for i, sl_1 in enumerate(sl_a):
    ss = opt.StateSpace([50., 10., 10.], [sl_1 * 50., 8., 8.], 0.)
    res = lperiod.get_allocation(ss)
    this_result = pd.DataFrame(res, columns=[i], index=['alloc A', 'alloc B','alloc C']).T
    cost = lperiod.get_cost(ss)
    this_result['cost']= cost
    this_result['sl'] = sl_1
    results = results.append(this_result)
    
duration = time.time() - start
