# -*- coding: utf-8 -*-
"""
Created on Mon Aug 14 15:22:25 2017

@author: kok29wv
"""

import framework.demand_generation as dg
import numpy as np
import matplotlib.pyplot as plt

allocation = 10
inventory = 0
periods= 100

demands = dg.generate_demands(periods,10,0.01, 10000, mode='sym')
cum_demands = np.cumsum(demands, axis=1)
allocations = np.ones_like(demands) * allocation
allocations[:,0] = allocations[:,0] + inventory
cum_allocations = np.cumsum(allocations, axis=1)
inventory = np.zeros_like(cum_allocations)
inventory[:,1:]=cum_allocations[:,:-1] - cum_demands[:,:-1]
available_for_fulfillment = np.maximum(allocations + inventory,0)
fulfilled = np.minimum(available_for_fulfillment, demands)

cum_fulfilled = np.cumsum(fulfilled, axis=1)


service_levels = cum_fulfilled/cum_demands
service_levels[np.isnan(service_levels)] = 1.

# Create a figure instance
fig = plt.figure()
# Create an axes instance
ax = fig.add_subplot(111)
# Create the boxplot
bp = ax.boxplot(service_levels, showmeans=True)
plt.tight_layout()
#
plt.figure()
plt.plot(range(periods), np.average(inventory, axis=0))
plt.plot(range(periods), np.average(fulfilled, axis=0))
plt.plot(range(periods), np.average(demands, axis=0))
#plt.plot(range(periods), np.median(service_levels, axis=0))

# Create a figure instance
fig = plt.figure()
# Create an axes instance
ax = fig.add_subplot(111)
# Create the boxplot
bp = ax.boxplot(inventory, showmeans=True)
plt.tight_layout()
#