# -*- coding: utf-8 -*-
"""
Created on Wed Aug 02 08:27:26 2017

@author: kok29wv
"""

import framework.objects as opt
import numpy as np
# import matplotlib.pyplot as plt
# import seaborn as sns
import framework.period
import pandas as pd
import time
# define customers


customers = [opt.Customer(0.9, 1000, 10, 2), opt.Customer(0.9, 1000, 10, 2)]

sl_a = np.linspace(0, 1, 25)
costs = list()
allocations = list()
results = pd.DataFrame()
speed = pd.DataFrame()
lperiod = framework.period.LastPeriod(customers, 10)
for t_method in ['auto3', 'brentq', 'auto', 'auto2', 'newton']:
    lperiod.reset()
    lperiod.method = t_method
    start = time.time()
    for i, sl_1 in enumerate(sl_a):
        ss = opt.StateSpace([10., 10.], [sl_1 * 10., 9.], 0.)
        res = lperiod.get_allocation(ss)
        cost = lperiod.get_cost(ss)
        results = results.append(pd.DataFrame([[t_method, sl_1]+res+[cost]], columns=['method', 'sl', 'alloc A',
                                                                                   'alloc B',
                                                                               'cost'
                                                                               ], index=[i]))
    duration = time.time()-start
    speed = speed.append(pd.DataFrame([[t_method, duration]], columns=['method', 'time']))


allocs = results.set_index('method', append=True)
allocs = allocs.unstack(-1)