import pickle
import warnings

import numpy as np
from scipy import stats as stats, integrate as scint


class NumericIntegration(object):
    def __init__(self, distribution, n=1000):
        """
        Initilizes the equiprobable points used for integration
        Parameters
        ----------
        n:
            number of points

        """
        self.distribution = distribution
        if isinstance(distribution.dist, stats._continuous_distns.norm_gen):
            intervals, points = self._init_normal_distribution(distribution.mean(), distribution.std(), n)
        else:
            intervals, points = self._init_any_distribution(distribution, n)

        mapping = points >= 0
        points = points[mapping]
        self.integration_intervals = intervals[mapping]
        self.integration_points = points
        self.integration_weight = 1. / len(points)
        self.interval_min = np.min(self.integration_intervals[~ np.isinf(self.integration_intervals)])
        self.interval_max = np.max(self.integration_intervals[~ np.isinf(self.integration_intervals)])

    def _init_normal_distribution(self, mean, std, n):
        path = "framework/utils/norm_{}.pkl".format(n)
        try:
            file = open(path, 'rb')
            intervals, points = pickle.load(file)
            file.close()
        except IOError:
            intervals, points = self._init_any_distribution(stats.norm(), n)
            file = open(path, 'wb')
            pickle.dump((intervals, points), file)
            file.close()
        points = points * std + mean
        intervals = intervals * std + mean
        return intervals, points

    @staticmethod
    def _init_any_distribution(distribution, n):
        intervals = distribution.ppf(np.linspace(0, 1, num=n + 1))
        points = np.zeros(n)

        for i in range(n):
            points[i] = n * scint.quad(lambda x: x * distribution.pdf(x), intervals[i], intervals[i + 1])[0]

        return intervals[0:-1], points

    def integrate(self, func, start=None, end=None, mode='interpolate'):
        if (start is not None) and (end is not None):
            raise ValueError("Integration on an interval is currently not supported")

        if start is None and end is None:
            return self.integration_weight * np.sum(func(self.integration_points))

        if end is not None:
            if np.isnan(end):
                raise ValueError('end may not be nan')
            if end >= self.interval_max:  # check if end is within the limits
                return self.integration_weight * np.sum(func(self.integration_points))
            if end <= self.interval_min:
                return 0.
            last_pos = np.argmax(self.integration_intervals > end)
            smaller_integral = self.integration_weight * np.sum(func(self.integration_points[:last_pos - 1]))
            if mode == 'interpolate':
                larger_integral = smaller_integral + \
                                  self.integration_weight * func(self.integration_points[last_pos])
                result = np.interp(end,
                                   [self.integration_intervals[last_pos - 1], self.integration_intervals[last_pos]],
                                   [smaller_integral, larger_integral])
                return result
            elif mode == 'integrate':
                return smaller_integral + scint.quad(lambda x: func(x) * self.distribution.pdf(x), start,
                                                     self.integration_intervals[last_pos])[0]
            elif mode == 'unsmooth':
                return smaller_integral
        else:  # start is not none!
            if np.isnan(start):
                raise ValueError('start may not be nan')
            if start >= self.interval_max:  # check if start is within the limits
                #warnings.warn("Value {} to large for integration points".format(start))
                return 0.
            if start <= self.interval_min:  # starting point below the limits -> Return the full integral
                return self.integration_weight * np.sum(func(self.integration_points))
            last_pos = np.argmax(self.integration_intervals > start)
            smaller_integral = self.integration_weight * np.sum(func(self.integration_points[last_pos:]))
            if mode == 'interpolate':
                larger_integral = smaller_integral + \
                                  self.integration_weight * func(self.integration_points[last_pos - 1])

                result = np.interp(start,
                                   [self.integration_intervals[last_pos - 1], self.integration_intervals[last_pos]],
                                   [larger_integral, smaller_integral])
                return result
            if mode == 'interpolate_man':
                larger_integral = smaller_integral + \
                                  self.integration_weight * func(self.integration_points[last_pos - 1])
                result = interpolate(start,
                                     self.integration_intervals[last_pos - 1], self.integration_intervals[last_pos],
                                     larger_integral, smaller_integral)
                return result
            elif mode == 'integrate':
                return smaller_integral + scint.quad(lambda x: func(x) * self.distribution.pdf(x), start,
                                                     self.integration_intervals[last_pos])[0]
            elif mode == 'unsmooth':
                return smaller_integral


def interpolate(x, x0, x1, y0, y1):
    return y0 + (y1 - y0) / (x1 - x0) * (x - x0)