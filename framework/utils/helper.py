import numpy as np
import pandas as pd


def supply_netting(supply, demand, ip=0.):
    """
    caluclates the supply available for allocation for each period

    Parameters
    ----------
    supply: np-array
        the supply vector
    demand : np-array
        the demand vector
    ip :
        the inventory position in the beginning of the first period

    Returns
    -------
    np-array:
        the available supply vector

    """
    available_supply = np.zeros_like(supply, dtype=np.float)
    available_supply[0] = supply[0] + ip
    for t in range(1, len(supply)):
        available_supply[t] = available_supply[t - 1] - np.sum(demand[:, t - 1]) + supply[t]

    return available_supply


def calculate_penalty(service_levels, penalty, demand, allocations):
    """
    calculates the realized penalty for a given demand realization
    Parameters
    ----------
    service_levels : float_array n x 1
    penalty : float_array n x 1
    demand : float_array with n x t
    allocations : float_array with n x t

    Returns
    -------
    (float) the total penalty
    """

    filled_demand = np.sum(np.minimum(allocations, demand), axis=1)
    total_demand = np.sum(demand, axis=1)
    actual_sl = filled_demand / total_demand
    penalties = penalty * np.maximum(service_levels - actual_sl, 0)
    return np.sum(penalties)


def backorders(allocations, demands):
    return np.maximum(demands - allocations, 0)


def fulfilled(allocations, demands):
    return np.minimum(allocations, demands)


def service_levels(allocations, demands):
    return np.sum(fulfilled(allocations, demands), axis=1) / np.sum(demands, axis=1)


def longform(array, axnames, valuename='Value'):
    if len(array.shape) > 2:
        result = pd.DataFrame()
        for i in range(array.shape[0]):
            temp_df = longform(array[i, :], axnames=axnames[1:], valuename=valuename)
            temp_df[axnames[0]] = i
            result = result.append(temp_df)
        result.set_index(axnames[0], append=True, inplace=True)
        return result
    elif len(array.shape) == 2:
        result = pd.DataFrame(array)
        result.index.names = [axnames[0]]
        result.columns.names = [axnames[1]]
        result = pd.DataFrame(result.stack())
        result.columns = [valuename]
        return result
    else:
        return pd.DataFrame(array, columns=[valuename])


def array_to_str(array, formatstr='5.2f'):
    result = '['
    for i in range(len(array) - 1):
        result += "{:{val}}, ".format(array[i], val=formatstr)
    result += "{:{val}}]".format(array[-1], val=formatstr)
    return result
