# -*- coding: utf-8 -*-
import pickle
import os.path
from copy import deepcopy


class Pickle(object):
    save_folder = "ex_data"

    def __init__(self, ident, mode='w'):
        self.id = ident
        try:
            f = self._open('r')
        except IOError:
            f=self._open('w')
            f.close()
            f = self._open('r')
        try:
            self._data = pickle.load(f)
        except EOFError:
            self._data = dict()
    

        f.close()

    def _open(self, mode):
        path = os.path.abspath(os.path.join(self.save_folder, 'exp_{}.pk'.format(self.id)))
        return open(path, mode)

    def __getitem__(self, item):
        return deepcopy(self._data[item])

    def __setitem__(self, key, value):
        self._data[key] = value

    def __delitem__(self, key):
        del self._data[key]

    def __iter__(self):
        return iter(self._data)

    def __len__(self):
        return len(self._data)

    def __repr__(self):
        result = ""
        for t_key in self.iterkeys():
            result += "------------\n  {}\n------------\n{}\n".format(t_key, self[t_key])
        return result

    def iterkeys(self):
        return self._data.iterkeys()

    def keys(self):
        return self._data.keys()

    def save(self):
        f = self._open('w')
        pickle.dump(self._data, f)
        f.close()
