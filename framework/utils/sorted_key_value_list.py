from __future__ import print_function

from pyutilib.enum import Enum


class Range(Enum):
    """
    Enum for defining if a Value is ranged value or not.

    Attributes
    ----------
    eq = 0
        Value is a ranged value from 0 till Value (including).
    le = 1
        Value is a single value.
    """
    eq = 0
    le = 1


class RangeValue(object):
    """
    Value which can be ranged, with custom equality comparison.
    """

    def __init__(self, value, range_value=Range.eq):
        """

        Parameters
        ----------
        value : float
            Value to be stored as RangedValue.
            If the value is negative it is stored as Range.le value and any given range_value is overridden.
        range_value : Range
            Range for this value.
            Is overridden if the given value is negative.
            Default is Range.eq.
        """
        if isinstance(value, RangeValue):
            range_value = value.range
            value = value.value
        elif value < 0:
            value = abs(value)
            range_value = Range.le
        self._value = value
        self._range = range_value

    def __repr__(self):
        if self._range is Range.eq:
            return str(self._value)
        elif self._range is Range.le:
            return "<=" + str(self._value)
        else:
            raise ValueError("Unexpected range value: " + str(self._range))

    def __lt__(self, other):
        """
        Notes
        -----
            A ranged value is greater as a single value of equal numbers.

        Parameters
        ----------
        other : RangeValue
            RangeValue object to compare to.

        Returns
        -------
        bool
            If this object is lesser then the other.
        """
        if self.value < other.value:
            return True
        elif self.value == other.value and self.range is Range.eq and other.range is Range.le:
            return True
        else:
            return False

    def __gt__(self, other):
        """
        Notes
        -----
            A ranged value is greater as a single value of equal numbers.

        Parameters
        ----------
        other : RangeValue
            RangeValue object to compare to.

        Returns
        -------
        bool
            If this object is greater then the other.
        """
        if self.value > other.value:
            return True
        elif self.value == other.value and self.range is Range.le and other.range is Range.eq:
            return True
        else:
            return False

    def __eq__(self, other):
        """
        Notes
        -----
            A ranged value is greater as a single value of equal numbers.

        Parameters
        ----------
        other : RangeValue
            RangeValue object to compare to.

        Returns
        -------
        bool
            If this object is equal to the other.
        """
        return self.value == other.value and self.range is other.range or self.value < other.value and other.range is Range.le

    def __le__(self, other):
        """
        Notes
        -----
            A ranged value is greater as a single value of equal numbers.

        Parameters
        ----------
        other : RangeValue
            RangeValue object to compare to.

        Returns
        -------
        bool
            If this object is lesser then or equal to the other.
        """
        return self.__eq__(other) or self.__lt__(other) and other.range is Range.le

    def __ge__(self, other):
        """
        Notes
        -----
            A ranged value is greater as a single value of equal numbers.

        Parameters
        ----------
        other : RangeValue
            RangeValue object to compare to.

        Returns
        -------
        bool
            If this object is greater then or equal to the other.
        """
        return self.__eq__(other) or self.__gt__(other) and other.range is Range.eq

    def __cmp__(self, other):
        """
        Compares this object to another RangedValue.

        Parameters
        ----------
        other : RangedValue
            RangedValue to compare to.

        Returns
        -------
        int
            Negative if lesser; Positive if greater; Zero if equal.
        """
        if self < other:
            return -1
        elif self > other:
            return 1
        else:
            return 0

    @property
    def range(self):
        """
        The Range for this value.
        """
        return self._range

    @range.setter
    def range(self, range_value):
        self._range = range_value

    @property
    def value(self):
        """
        The float value to store as ranged value.
        """
        return self._value

    @value.setter
    def value(self, value):
        self._value = value


class KeyValue(object):
    """
    Key - value pair.
    """

    def __init__(self, key, value):
        """

        Parameters
        ----------
        key : float
            The key to the value.
        value : object
            The value to the given key.
        """
        self._key = key
        self._value = value

    def __repr__(self):
        return "KeyValue({}, {})".format(self._key, self._value)

    @property
    def key(self):
        """
        The key (float).
        """
        return self._key

    @key.setter
    def key(self, new_key):
        if not isinstance(new_key, RangeValue):
            raise TypeError("RangeValue expected as key!")
        self._key = new_key

    @property
    def value(self):
        """
        The value (list|float)
        """
        return self._value

    @value.setter
    def value(self, new_value):
        self._value = new_value

    def __cmp__(self, other):
        """
        Comparing KeyValue's by their key's.

        Parameters
        ----------
        other : KeyValue
            KeyValue to compare to.

        Returns
        -------
        int
            Same as the default.
        """
        return self.key.__cmp__(other.key)

    def __lt__(self, other):
        return self.key < other.key

    def __gt__(self, other):
        return self.key > other.key

    def __eq__(self, other):
        return self.key == other.key

    def __le__(self, other):
        return self.key <= other.key

    def __ge__(self, other):
        return self.key >= other.key


class SortedKeyValueList:
    """
    A multi dimensional ordered list.

    Notes
    -----
        The list is ordered according to the order established by the KeyValue objects.
    """

    def __init__(self):
        self._list = []

    def __repr__(self):
        res = ''
        for item in self:
            res += "{}: {}\n".format(item[0], item[1])
        return res

    def __iter__(self):
        """

        Returns
        -------
        iter
            Depth first iterator.
        """
        return self._iter(self._list)

    def _iter(self, current, result_list=list()):
        """
        Used to recursively generate the iterator.

        Parameters
        ----------
        current : RangeValue | list
            Item currently expanded.
        result_list : list
            The current result list.

        Returns
        -------
        iter
            Iterator over the SortedKeyValueList object.
        """
        if isinstance(current, list) and isinstance(current[0], KeyValue):
            for ele in current:
                new_result_list = result_list + [ele.key]
                for result in self._iter(ele.value, new_result_list):
                    yield result
        else:
            yield [tuple(result_list), current]

    def append(self, vector, result):
        """
        Appends a vector with result to the SortedKeyValueList object.

        Parameters
        ----------
        vector : list | tuple
            Vector to add.
        result : float
            Result to add.

        Returns
        -------

        """
        vector = [RangeValue(value) for value in vector]
        SortedKeyValueList._append(vector=vector, result=result, key_value_list=self._list)

    @staticmethod
    def _append(vector, result, key_value_list):
        """
        Recursive method for appending vector and result.

        Parameters
        ----------
        vector : list
            Vector to add.
        result : float
            Result to add.
        key_value_list : list
            Current list.

        Returns
        -------

        """
        v1 = vector[0]
        if len(vector) > 1:
            vector = vector[1:]
            if SortedKeyValueList._contains_key(key_value_list=key_value_list, key=v1):
                new_key_value = SortedKeyValueList._get_key(key=v1, key_value_list=key_value_list)
                SortedKeyValueList._append(vector=vector, result=result, key_value_list=new_key_value.value)
            else:
                new_key_value = KeyValue(v1, [])
                key_value_list.append(new_key_value)
                key_value_list.sort()
                SortedKeyValueList._append(vector=vector, result=result, key_value_list=new_key_value.value)
        else:
            if SortedKeyValueList._contains_key(key_value_list=key_value_list, key=v1):
                return
            else:
                key_value_list.append(KeyValue(v1, result))
                key_value_list.sort()

    def get(self, vector):
        """
        Method for getting a result to the given vector.

        Parameters
        ----------
        vector : list | tuple
            Vector to get the result to.

        Returns
        -------
        float | None
            The result or None.
        """
        vector = [RangeValue(value) for value in vector]
        return SortedKeyValueList._get(vector=vector, key_value_list=self._list)

    @staticmethod
    def _get(vector, key_value_list):
        """
        Method to recursively getting the result to the given vector.

        Parameters
        ----------
        vector : list
            Vector to get the result for.
        key_value_list : list
            Current list.

        Returns
        -------
        float | None
            The result or None.
        """
        v1 = vector[0]
        if len(vector) > 1:
            vector = vector[1:]
            if SortedKeyValueList._contains_key_match(key_value_list, v1):
                for key_value in SortedKeyValueList._get_key_match(v1, key_value_list):
                    return SortedKeyValueList._get(vector=vector, key_value_list=key_value.value)
            else:
                pass
        else:
            key_value = list(SortedKeyValueList._get_key_match(v1, key_value_list))
            if len(key_value) is 1:
                return key_value[0].value
            elif len(key_value) > 1:
                raise ValueError("Multiple results found for this Vektor!")
            else:
                return None

    @staticmethod
    def _contains_key(key_value_list, key):
        """
        Checks if the key is contained in the given list.

        Notes
        -----
            Ignores the custom comparison methods!

        Parameters
        ----------
        key_value_list : list
            The list to check.
        key : float
            The key to look for.

        Returns
        -------
        bool
            If the list contains the given key.
        """
        for element in key_value_list:
            if key.value == element.key.value and key.range is element.key.range:
                return True
        return False

    @staticmethod
    def _get_key(key, key_value_list):
        """
        Gets the KeyValue element to a given key of the given list.

        Notes
        -----
            Ignores the custom comparison methods!

        Parameters
        ----------
        key : float
            The key to look for.
        key_value_list : list
            The list to look in.

        Returns
        -------
        KeyValue
            The KeyValue object or None.
        """
        for element in key_value_list:
            if key.value == element.key.value and key.range is element.key.range:
                return element
        return None

    @staticmethod
    def _contains_key_match(key_value_list, key):
        """
        Checks if the key is contained in the given list.

        Parameters
        ----------
        key_value_list : list
            The list to check.
        key : float
            The key to look for.

        Returns
        -------
        bool
            If the list contains the given key.
        """
        for element in key_value_list:
            if key == element.key:
                return True
        return False

    @staticmethod
    def _get_key_match(key, key_value_list):
        """
        Gets the KeyValue element to a given key of the given list.

        Parameters
        ----------
        key : float
            The key to look for.
        key_value_list : list
            The list to look in.

        Returns
        -------
        iter
            Iterator over the matching KeyValue objects or None.
        """
        for element in key_value_list:
            if key == element.key:
                yield element


if __name__ == '__main__':
    test_vector_list = [
        (1, 2, -3),
        (3, 2, 1),
        (1, 1, 1),
        (2, -3, 1),
        (1, 1, 2)
    ]
    slist = SortedKeyValueList()
    for i, t in enumerate(test_vector_list):
        slist.append(t, [i])
    print("repr")
    print(slist.__repr__())
