# -*- coding: utf-8 -*-
from framework.utils.data_interface import Pickle
import framework.lp_allocation as lp
from framework.structure import Setup
import pandas as pd
import numpy as np


def expost_from_id(scenario_id):
    p = Pickle(scenario_id, 'r')

    s = Setup.from_dict(p['setup'])
    demands = p['demands']
    results, allocations = run_expost(s, demands)

    agg_data = results.describe()
    agg_data['method'] = 'expost'
    agg_data.set_index('method', append=True, inplace=True)

    try:
        agg_results = p['results']
    except KeyError:
        agg_results = pd.DataFrame()
    agg_results = agg_results.append(agg_data)

    p["agg_results"] = agg_results
    p["expost_alloc"] = allocations
    p.save()

    return agg_results


def run_expost(s, demands):
    """
    runs an expost simulation
    Parameters
    ----------
    s: Setup-Object
        The setup of the experiment
    demands: np-array
        the demand realizations (dimensions n x t x realizations)

    Returns
    -------
    pd_results : pandas Dataframe
        the Penalty and Runtime
    allocations : np-array
        the allocation for each period
    """
    experiments = demands.shape[2]
    allocations = np.zeros((s.n, s.t, experiments))
    pd_results = pd.DataFrame()
    for ex in range(experiments):
        m, allocations[:, :, ex], _ = lp.solve_model(demands[:, :, ex], s.supply, s.sl, s.pen)
        this_result = pd.DataFrame([[m.ObjVal, m.Runtime]], columns=['Penalty', 'Runtime'], index=[ex])
        pd_results = pd_results.append(this_result)
    pd_results['Method'] = 'expost'
    return pd_results, allocations


if __name__ == "__main__":
    result = expost_from_id(1000)
