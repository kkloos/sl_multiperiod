# -*- coding: utf-8 -*-
import numpy as np
import pandas as pd
import framework.lp_allocation as lp
import time
import framework.utils.helper as hlp


def run_lp_approx(s, demands):
    """

    Parameters
    ----------
    s : the setup object
    demands : the demand array

    Returns
    -------
    pd_results : pandas Dataframe
        the Penalty and Runtime
    allocations : np-array
        the allocation for each period
    """
    experiments = demands.shape[2]
    allocation = np.zeros(demands.shape)
    pd_results = pd.DataFrame()

    # solve the initial allocation

    m, this_result, unallocated = lp.solve_model(s.demand, s.supply, s.sl, s.pen)
    initial_allocation = this_result[:, 0] + unallocated[0] * s.demand[:, 0] / np.sum(s.demand[:, 0])

    for t_ex in range(experiments):
        start = time.time()
        allocation[:, 0, t_ex] = initial_allocation
        total_demand = demands[:, 0, t_ex]
        fulfilled_demand = np.minimum(total_demand, allocation[:, 0, t_ex])
        ip = s.supply[0] - np.sum(total_demand)
        for t_period in range(1, s.t):
            # if the available supply is negative, no allocation must be made in the current period.
            if ip + s.supply[t_period] <= 0:
                allocation[:, t_period, t_ex] = np.zeros(s.n)
            else:
                m, this_allocation, unallocated = lp.solve_model(s.demand[:, t_period:], s.supply[t_period:], s.sl,
                                                              s.pen, ip,
                                                              total_demand, fulfilled_demand)
                # as the lp never allocates more than mean demand, we allocate the remaining supply on a per commit
                # basis
                leftover = unallocated[0] * s.demand[:, t_period] / np.sum(
                    s.demand[:, t_period])
                allocation[:, t_period, t_ex] = this_allocation[:, 0] + leftover

            total_demand = total_demand + demands[:, t_period, t_ex]
            fulfilled_demand += np.minimum(demands[:, t_period, t_ex], allocation[:, t_period, t_ex])
            ip += s.supply[t_period] - np.sum(demands[:, t_period, t_ex])

        penalty = hlp.calculate_penalty(s.sl, s.pen, demands[:, :, t_ex], allocation[:, :, t_ex])
        runtime = time.time() - start
        this_result = pd.DataFrame([[penalty, runtime]], columns=['Penalty', 'Runtime'], index=[t_ex])
        pd_results = pd_results.append(this_result)

    pd_results['Method'] = 'lpapprox'
    return pd_results, allocation
