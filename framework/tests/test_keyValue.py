from unittest import TestCase

from sl_multiperiod.framework.utils.sorted_key_value_list import KeyValue, RangeValue, Range

o1 = KeyValue(RangeValue(1, Range.eq), "o2")
o2 = KeyValue(RangeValue(1, Range.eq), "o2")
o3 = KeyValue(RangeValue(1, Range.le), "o3")
o4 = KeyValue(RangeValue(1, Range.le), "o4")
o5 = KeyValue(RangeValue(2, Range.le), "o5")
o6 = KeyValue(RangeValue(2, Range.le), "o6")
o7 = KeyValue(RangeValue(3, Range.eq), "o7")


class TestKeyValue(TestCase):
    def test_gt(self):
        # positiv tests
        self.assertTrue(o7 > o1, "3 > 1")
        self.assertTrue(o7 > o5, "3 > -2")
        self.assertTrue(o5 > o3, "-2 > -1 : "+str(o5 > o3))
        self.assertTrue(o3 > o1, "-1 > 1")
        # negativ tests
        self.assertFalse(o1 > o2, "1 > 1")
        self.assertFalse(o1 > o7, "1 > 3")
        self.assertFalse(o2 > o3, "1 > -1")
        self.assertFalse(o1 > o5, "1 > -2")
        self.assertFalse(o3 > o4, "-1 > -1")
        self.assertFalse(o3 > o5, "-1 > -2")
        self.assertFalse(o3 > o7, "-1 > 3")

    def test_ge(self):
        # positiv tests
        self.assertTrue(o7 >= o1, "3 >= 1")
        self.assertTrue(o1 >= o2, "1 >= 1")
        self.assertTrue(o7 >= o5, "3 >= -2")
        self.assertTrue(o5 >= o3, "-2 >= -1")
        self.assertTrue(o3 >= o4, "-1 >= -1")
        self.assertTrue(o3 >= o1, "-1 >= 1")
        # negativ tests
        self.assertFalse(o1 >= o7, "1 >= 3")
        self.assertFalse(o2 >= o3, "1 >= -1")
        self.assertFalse(o1 >= o5, "1 >= -2")
        self.assertFalse(o3 >= o5, "-1 >= -2")
        self.assertFalse(o3 >= o7, "-1 >= 3")

    def test_lt(self):
        # positiv tests
        self.assertTrue(o1 < o7, "1 < 3")
        self.assertTrue(o5 < o7, "-2 < 3")
        self.assertTrue(o3 < o5, "-1 < -2")
        self.assertTrue(o1 < o3, "1 < -1")
        # negativ tests
        self.assertFalse(o2 < o1, "1 < 1")
        self.assertFalse(o7 < o1, "3 < 1")
        self.assertFalse(o3 < o2, "-1 < 1")
        self.assertFalse(o5 < o1, "-2 < 1")
        self.assertFalse(o4 < o3, "-1 < -1")
        self.assertFalse(o5 < o3, "-2 < -1")
        self.assertFalse(o7 < o3, "3 < -1")

    def test_le(self):
        # positiv tests
        self.assertTrue(o2 <= o1, "1 <= 1")
        self.assertTrue(o1 <= o7, "1 <= 3")
        self.assertTrue(o5 <= o7, "-2 <= 3")
        self.assertTrue(o4 <= o3, "-1 <= -1")
        self.assertTrue(o3 <= o5, "-1 <= -2")
        self.assertTrue(o1 <= o3, "1 <= -1")
        # negativ tests
        self.assertFalse(o7 <= o1, "3 <= 1")
        self.assertFalse(o3 <= o2, "-1 <= 1")
        self.assertFalse(o5 <= o1, "-2 <= 1")
        self.assertFalse(o5 <= o3, "-2 <= -1")
        self.assertFalse(o7 <= o3, "3 <= -1")

    def test_eq(self):
        # positiv tests
        self.assertTrue(o2 == o1, "1 == 1")
        self.assertTrue(o4 == o3, "-1 == -1")
        # negativ tests
        self.assertFalse(o7 == o1, "3 == 1")
        self.assertFalse(o1 == o7, "1 == 3")
        self.assertFalse(o5 == o7, "-2 == 3")
        self.assertFalse(o3 == o2, "-1 == 1")
        self.assertFalse(o5 == o1, "-2 == 1")
        self.assertFalse(o5 == o3, "-2 == -1")
        self.assertFalse(o7 == o3, "3 == -1")
        self.assertFalse(o3 == o5, "-1 == -2")
        self.assertFalse(o1 == o3, "1 == -1")
