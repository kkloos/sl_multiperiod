from unittest import TestCase

from sl_multiperiod.framework.utils.sorted_key_value_list import RangeValue, Range


class TestRangeValue(TestCase):
    def test_range(self):
        rv0 = RangeValue(0, Range.eq)
        rv1 = RangeValue(1, Range.eq)
        rv2 = RangeValue(2, Range.eq)
        rv1le = RangeValue(1, Range.le)
        rv2le = RangeValue(2, Range.le)
        self.assertTrue(rv1le < rv2)
        self.assertTrue(rv1 < rv2)
        self.assertTrue(rv1 < rv2le)
        self.assertTrue(rv1 < rv1le)
        self.assertFalse(rv1 < rv0)
        self.assertFalse(rv1 < rv1)
        self.assertFalse(rv2 < rv1le)
        self.assertFalse(rv1le < rv1le)
        self.assertFalse(rv1le < rv0)
        self.assertEqual(rv1, rv1)
        self.assertEqual(rv1, rv1le)
        self.assertEqual(rv1, rv2le)
        self.assertEqual(rv1le, rv2le)
        self.assertNotEqual(rv1, rv2)
        self.assertNotEqual(rv1le, rv2)

    def test_value(self):
        rv1 = RangeValue(1, Range.le)
        self.assertEqual(rv1.value, 1)
        self.assertEqual(rv1.range, Range.le)
        rv1.value = 2
        self.assertEqual(rv1.value, 2)
        rv1.range = Range.eq
        self.assertEqual(rv1.range, Range.eq)
