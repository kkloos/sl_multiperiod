from __future__ import print_function
from unittest import TestCase
from framework.utils.sorted_key_value_list import KeyValue, SortedKeyValueList, RangeValue, Range


class TestSortedKeyValueList(TestCase):
    def test_append(self):
        # Test level 1
        sorted_list = SortedKeyValueList()
        self.assertEqual(sorted_list._list, [])

        sorted_list.append([RangeValue(1)], "1")
        self.assertEqual(sorted_list._list, [KeyValue(RangeValue(1), "1")])

        sorted_list.append([RangeValue(1, Range.le)], "<=1")
        self.assertEqual(sorted_list._list, [KeyValue(RangeValue(1), "1"), KeyValue(RangeValue(1, Range.le), "<=1")])

        with self.assertRaises(Exception):
            sorted_list.append([RangeValue(1, Range.le)], "<=1")

    def test_append_level_2(self):
        # Test level 2
        sorted_list = SortedKeyValueList()
        sorted_list.append([1, 1], "Result 1") # -> ok
        self.assertEqual(sorted_list._list, [KeyValue(RangeValue(1), KeyValue(RangeValue(1), "1"))])
        sorted_list.append([1, 1], "Result 1.2") # -> no
        self.assertEqual(sorted_list._list, [KeyValue(RangeValue(1), KeyValue(RangeValue(1), "1"))])

        sorted_list.append([-1, 1], "Result 2")

        sorted_list.append([-5, 1], "Result 2")

        sorted_list.append([5, 2], "Result 5.2") # ->ok
        sorted_list.append([5, 3], "Result 5.2") # ->ok

        print(sorted_list)

    def test_get(self):
        second_dim = [KeyValue(RangeValue(0), "0"), KeyValue(RangeValue(1), "1")]
        test_list_two_dim = [KeyValue(RangeValue(1, Range.le), second_dim), KeyValue(RangeValue(3), second_dim),
                             KeyValue(RangeValue(4), second_dim), KeyValue(RangeValue(5), second_dim)]

        sorted_list = SortedKeyValueList()

        sorted_list._list = test_list_two_dim
        self.assertEqual(sorted_list.get((RangeValue(0), RangeValue(0))), "0")
        self.assertEqual(sorted_list.get((RangeValue(1), RangeValue(1))), "1")
        self.assertEqual(sorted_list.get((RangeValue(2), RangeValue(0))), None)
        self.assertEqual(sorted_list.get((RangeValue(1), RangeValue(2))), None)

        with self.assertRaises(Exception):
            sorted_list.get((RangeValue(0), RangeValue(0), RangeValue(0)))

        sorted_list._list = second_dim
        self.assertEqual(sorted_list.get([RangeValue(0)]), "0")
        self.assertEqual(sorted_list.get([RangeValue(1)]), "1")
        self.assertEqual(sorted_list.get([RangeValue(2)]), None)

    def test__contains_key(self):
        test_list1 = [KeyValue(RangeValue(0, Range.eq), "0"), KeyValue(RangeValue(1, Range.le), "1"),
                      KeyValue(RangeValue(3, Range.eq), "3"), KeyValue(RangeValue(4, Range.eq), "4"),
                      KeyValue(RangeValue(5, Range.eq), "5"), KeyValue(RangeValue(5, Range.le), "5")]
        self.assertTrue(SortedKeyValueList._contains_key(test_list1, RangeValue(3, Range.eq)))
        self.assertFalse(SortedKeyValueList._contains_key(test_list1, RangeValue(3, Range.le)))
        self.assertFalse(SortedKeyValueList._contains_key(test_list1, RangeValue(1, Range.eq)))
        self.assertTrue(SortedKeyValueList._contains_key(test_list1, RangeValue(1, Range.le)))
        self.assertTrue(SortedKeyValueList._contains_key(test_list1, RangeValue(5, Range.eq)))
        self.assertTrue(SortedKeyValueList._contains_key(test_list1, RangeValue(5, Range.le)))
        self.assertFalse(SortedKeyValueList._contains_key(test_list1, RangeValue(7, Range.eq)))
        self.assertFalse(SortedKeyValueList._contains_key(test_list1, RangeValue(0, Range.le)))

    def test__get_key(self):
        test_list1 = [KeyValue(RangeValue(0, Range.eq), "0"), KeyValue(RangeValue(1, Range.le), "1"),
                      KeyValue(RangeValue(3, Range.eq), "3"), KeyValue(RangeValue(4, Range.eq), "4"),
                      KeyValue(RangeValue(5, Range.eq), "5"), KeyValue(RangeValue(5, Range.le), "5")]
        self.assertEqual(SortedKeyValueList._get_key(RangeValue(3, Range.eq), test_list1),
                         KeyValue(RangeValue(3, Range.eq), "3"))
        self.assertEqual(SortedKeyValueList._get_key(RangeValue(3, Range.le), test_list1), None)
        self.assertEqual(SortedKeyValueList._get_key(RangeValue(1, Range.eq), test_list1), None)
        self.assertEqual(SortedKeyValueList._get_key(RangeValue(1, Range.le), test_list1),
                         KeyValue(RangeValue(1, Range.le), "1"))
        self.assertEqual(SortedKeyValueList._get_key(RangeValue(5, Range.eq), test_list1),
                         KeyValue(RangeValue(5, Range.eq), "5"))
        self.assertEqual(SortedKeyValueList._get_key(RangeValue(5, Range.le), test_list1),
                         KeyValue(RangeValue(5, Range.le), "5"))

    def test__list_contains_key(self):
        test_list1 = [KeyValue(RangeValue(1, Range.le), "1"), KeyValue(RangeValue(3), "3"),
                      KeyValue(RangeValue(4), "4"), KeyValue(RangeValue(5), "5")]
        self.assertTrue(SortedKeyValueList._contains_key_match(test_list1, RangeValue(1)))
        self.assertTrue(SortedKeyValueList._contains_key_match(test_list1, RangeValue(0)))
        self.assertTrue(SortedKeyValueList._contains_key_match(test_list1, RangeValue(4)))
        self.assertTrue(SortedKeyValueList._contains_key_match(test_list1, RangeValue(0.5)))
        self.assertFalse(SortedKeyValueList._contains_key_match(test_list1, RangeValue(7)))
        self.assertTrue(SortedKeyValueList._contains_key_match(test_list1, RangeValue(0.5)))
        self.assertTrue(SortedKeyValueList._contains_key_match(test_list1, RangeValue(2)))

    def test__object_with_key_in_list(self):
        test_list1 = [KeyValue(RangeValue(0, Range.eq), "0"), KeyValue(RangeValue(1, Range.eq), "1"),
                      KeyValue(RangeValue(3, Range.eq), "3"), KeyValue(RangeValue(4, Range.eq), "4"),
                      KeyValue(RangeValue(5, Range.eq), "5")]
        self.assertEqual(list(SortedKeyValueList._get_key_match(RangeValue(0, Range.eq), test_list1)),
                         test_list1)
        self.assertEqual(list(SortedKeyValueList._get_key_match(RangeValue(4, Range.eq), test_list1)),
                         [KeyValue(RangeValue(4, Range.eq), "4"), KeyValue(RangeValue(5, Range.eq), "5")])
        self.assertEqual(list(SortedKeyValueList._get_key_match(RangeValue(7, Range.eq), test_list1)), [])
        self.assertEqual(list(SortedKeyValueList._get_key_match(RangeValue(0.5, Range.eq), test_list1)),
                         [KeyValue(RangeValue(1, Range.eq), "1"), KeyValue(RangeValue(3, Range.eq), "3"),
                          KeyValue(RangeValue(4, Range.eq), "4"),
                          KeyValue(RangeValue(5, Range.eq), "5")])
        self.assertEqual(list(SortedKeyValueList._get_key_match(RangeValue(2, Range.eq), test_list1)),
                         [KeyValue(RangeValue(3, Range.eq), "3"), KeyValue(RangeValue(4, Range.eq), "4"),
                          KeyValue(RangeValue(5, Range.eq), "5")])
