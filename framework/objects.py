import numpy as np
import scipy.optimize as opt
import scipy.stats as stats
import framework.utils.helper as hlp

import framework.objective as ob


class StateSpace(object):
    def __init__(self, demand, filled_demand, inventory):
        """ Creates a state space object.

        Parameters
        ----------
        demand : list
            list of total demand
        filled_demand : list
            list fo filled demand
        inventory : float
            carry over inventory
        """
        self.demand = np.array(demand)
        self.filled_demand = np.array(filled_demand)
        self.inventory = float(inventory)
        if len(self.demand) != len(self.filled_demand):
            raise ValueError("demand ({}} and filled demand ({}) must have same lenght!".format(len(self.demand),
                                                                                                len(
                                                                                                    self.filled_demand)))
        if any(self.demand < self.filled_demand):
            raise ValueError("Filled demand may not exceed total demand.")

        if any(self.filled_demand < 0):
            raise ValueError("Filled-demand must be non-negative, is: {}".format(self.filled_demand))

    def get_demands(self, id):
        """
        Returns the demand and filled demand of customer id
        Parameters
        ----------
        id : int
            id/ position of the customer

        Returns
        -------
            (demand, filled_demand) tuple of floats

        """
        return (self.demand[id], self.filled_demand[id])

    def update(self, demands, allocations, supply):
        filled_demand = np.minimum(demands, allocations)
        self.filled_demand += filled_demand
        self.demand += demands
        self.inventory = self.inventory + supply - np.sum(demands)

    def __len__(self):
        return len(self.demand)

    def __repr__(self):
        return "demand: {}\nfilled demand: {}\n sl: {}\ninventory: {}".format(hlp.array_to_str(self.demand),
                                                                              hlp.array_to_str(self.filled_demand),
                                                                              hlp.array_to_str(self.filled_demand /
                                                                                               self.demand),
                                                                              self.inventory)

    def to_vector(self):
        return np.concatenate((self.demand, self.filled_demand))


class Customer(object):
    def __init__(self, sl, pen, mean, std):
        """
        Creates a single customer
        Parameters
        ----------
        sl
            Service Level of the Customer
        pen
            Penalty of the Customer
        mean
            mean demand of the Customer
        std
            standard-deviation of the customer demand
        """
        self.distribution = stats.norm(loc=mean, scale=std)
        self.sl = float(sl)
        self.pen = pen
        if self.sl >= 1. or self.sl <= 0:
            raise ValueError("Service Level must be between 0 and 1, is {}".format(self.sl))
        if self.pen <= 0:
            raise ValueError("Penalty must be positive, is {}".format(self.pen))

    def __repr__(self):
        return "N({},{}), sl:{}, pen:{}".format(self.distribution.mean(), self.distribution.std(), self.sl, self.pen)


def optimal_allocation_2(supply, statespace, customers, method='auto'):
    """
    calculates the optimal allocation for the two-customer case
    Parameters
    ----------
    supply
        the supply in the current period
    statespace
        a StateSpace Object
    customers
        a list of customer Objects

    Returns
    -------
    (list_of_allocations, expected_cost)
        the optimal allocation as a list of allocations and the expected cost of this
    allocation

    """
    if len(customers) != 2 or len(statespace) != 2:
        raise ValueError("Instance has more than two customers")

    available_supply = supply + statespace.inventory

    # assemble the marginal cost function:

    def d_cost(alloc_1):
        return ob.ob_dcost(alloc_1, 0, statespace, customers[0]) - \
               ob.ob_dcost(available_supply - alloc_1, 1, statespace,
                           customers[1])

    def dd_cost(alloc_1):
        return ob.ob_ddcost(alloc_1, 0, statespace, customers[0]) + \
               ob.ob_ddcost(available_supply - alloc_1, 1, statespace,
                            customers[1])

    alloc = [0] * 2

    cost_red_0 = d_cost(0)
    cost_red_1 = d_cost(available_supply)
    # print "at supply {}: d_cost is {:.2f} and {:.2f}".format(available_supply, cost_red_0,cost_red_1)

    if cost_red_0 > 0:  # if the marginal cost of allocating nothing to customer 0 is positive, then allocate nothing to him.
        alloc[0] = 0
    elif cost_red_1 < 0:  # if the marginal cost of allocating everything to customer 0 is positive, allocate everything to him
        alloc[0] = available_supply
    else:
        if method == 'auto3':
            start_alloc_1 = (available_supply * cost_red_0) / (
                cost_red_0 - cost_red_1)  # linear approximation based on d_cost
            next_x = start_alloc_1 - (d_cost(start_alloc_1) / dd_cost(start_alloc_1))
            if 0 < next_x < available_supply:  # try if newton converges
                alloc[0] = opt.newton(d_cost, next_x, fprime=dd_cost)
            else:
                alloc[0] = opt.brentq(d_cost, 0, available_supply)
        elif method == 'brentq':
            alloc[0] = opt.brentq(d_cost, 0, available_supply)
        elif method == 'auto':
            initialization = opt.brentq(d_cost, 0, available_supply, xtol=available_supply / 20)
            alloc[0] = opt.newton(d_cost, initialization, fprime=dd_cost)
        elif method == 'auto2':
            initialization = opt.bisect(d_cost, 0, available_supply, xtol=available_supply / 20)
            alloc[0] = opt.newton(d_cost, initialization, fprime=dd_cost)
        elif method == 'newton':
            start_alloc_1 = (available_supply * cost_red_0) / (cost_red_0 - cost_red_1)
            alloc[0] = opt.newton(d_cost, start_alloc_1, fprime=dd_cost)
        else:
            raise ValueError('Method {} is not supported'.format(method))
    alloc[1] = available_supply - alloc[0]
    cost = sum((ob.ob_cost(alloc[i], i, statespace, customers[i]) for i in range(2)))

    return alloc, cost
