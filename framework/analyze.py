import pandas as pd
import framework.utils.helper as hlp


def make_df(allocation_dict, demands):
    result = pd.DataFrame()
    axnames = ['Customer', 'Period', 'Instance']
    for t_key in allocation_dict.keys():
        backorders = hlp.longform(hlp.backorders(allocation_dict[t_key], demands), axnames)
        filled = hlp.longform(hlp.fulfilled(allocation_dict[t_key], demands), axnames)
        sl = hlp.longform(hlp.service_levels(allocation_dict[t_key], demands), ['Period', 'Instance'])
        t_result = pd.concat([backorders, filled, sl], keys=['bo', 'filled', 'sl'], axis=0)
        t_result['Method'] = t_key
        result = result.append(t_result)

    return result


def sl_df(allocation_dict, demands):
    result = pd.DataFrame()
    axnames = ['Customer', 'Instance']
    for t_key in allocation_dict.keys():
        sl = hlp.longform(hlp.service_levels(allocation_dict[t_key], demands), axnames)
        sl['Method'] = t_key
        result = result.append(sl)

    result.reset_index(inplace=True)
    return result


def alloc_df(allocation_dict):
    result = pd.DataFrame()
    axnames = ['Customer', 'Period', 'Instance']
    for t_key in allocation_dict.keys():
        t_result = hlp.longform(allocation_dict[t_key], axnames)
        t_result['Method'] = t_key
        result = result.append(t_result)

    result.reset_index(inplace=True)
    return result
