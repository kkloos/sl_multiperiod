# -*- coding: utf-8 -*-
"""
Created on Mon Aug 14 15:22:44 2017

@author: kok29wv
"""
import scipy.stats as stats
import numpy as np

def generate_demands(periods, mean, standard_deviation, repetition=1000, mode='asym'):
    if mode=='asym':
        return np.maximum(stats.norm(loc=mean, scale=standard_deviation).rvs(size=(repetition, periods)),0)
    else:
        dat1 = np.maximum(stats.norm(loc=mean, scale=standard_deviation).rvs(size=(repetition/2, periods)),0)
        dat2 = 2.*mean - dat1
        return np.concatenate((dat1,dat2), axis=0)
