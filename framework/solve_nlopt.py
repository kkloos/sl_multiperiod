import nlopt
import numpy as np

# opt = nlopt.opt(nlopt.LD_SLSQP, 2)
#opt = nlopt.opt(nlopt.LD_SLSQP, 2)
# opt = nlopt.opt(nlopt.LD_CCSAQ, 2)
opt = nlopt.opt(nlopt.LD_MMA, 2)
# function = 0.5 x_1^3 + x_2^2


def f(x, grad):
    if grad.size > 0:
        grad[0] = float(3 * 0.5 * x[0] ** 2)
        grad[1] = float(2. * x[1])

    return 0.5 * x[0] ** 3. + x[1] ** 2


# constraint = 3x_1 + x_2 == 5
def h(x, grad):
    if grad.size > 0:
        grad[0] = 3.
        grad[1] = 1.

    return 3 * x[0] + x[1] - 5.


opt.set_min_objective(f)

opt.set_lower_bounds([0, 0])
opt.set_upper_bounds([10, 10])
opt.add_inequality_constraint(h)

opt.set_xtol_abs(1e-5)

xopt = opt.optimize([5., 5.])

opt_val = opt.last_optimum_value()
result = opt.last_optimize_result()

print xopt
print opt_val
print result
