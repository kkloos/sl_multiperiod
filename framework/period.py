# -*- coding: utf-8 -*-

import framework.objects as opt
import framework.cost_functions as cost
import scipy.optimize as optimize
import numpy as np
import logging
# import matplotlib.pyplot as plt
from copy import deepcopy
import nlopt
import framework.utils.helper as hlp
import framework.utils.integration
from framework.utils.sorted_key_value_list import SortedKeyValueList


class Period(object):
    """
    The Period Class objects holds the data and information for the decisions of a single period.
    """

    def __init__(self, customers, supply, integration_points=10000):
        """
        initilizes the Period Object which holds the customer information and the results of the optimizations
        Parameters
        ----------
        customers:
         a list of customer objects
        supply:
         a float of supply
        """
        self.customers = customers
        self.dimension = len(customers)
        self.input_states = dict()
        self.cost = dict()
        self.supply = supply
        self.state_list = dict()
        self.integration = [framework.utils.integration.NumericIntegration(t_cust.distribution, integration_points) for
                            t_cust in customers]

    def mean_demands(self):
        return np.array([t_cust.distribution.mean() for t_cust in self.customers])

    def expected_filled_demand(self, allocation):
        filled = np.zeros(self.dimension)
        mean = self.mean_demands()
        for i in range(self.dimension):
            part1 = self.integration[i].integrate(lambda xi: xi, end=allocation[i])
            part2 = allocation[i] * (1 - self.customers[i].distribution.cdf(allocation[i]))
            filled[i] = min(part1 + part2, mean[i])  # TODO: This is due to the unaccuracy in our integration
        result = filled
        return result

    def next_state(self, state, allocation):
        """
        Calculates the expected next state based on the given allocation
        Parameters
        ----------
        state
        allocation

        Returns
        -------
        a state space object
        """
        new_demand = state.demand + self.mean_demands()
        new_inventory = state.inventory + self.supply - np.sum(self.mean_demands())
        new_filled_demand = state.filled_demand + self.expected_filled_demand(allocation)

        return opt.StateSpace(new_demand, new_filled_demand, new_inventory)

    @staticmethod
    def _constraint(allocation, available_supply):
        const = available_supply - np.sum(allocation)
        # logging.debug("Remaining Supply: {} ({})".format(const, allocation))
        return const

    @staticmethod
    def _d_constraint(allocation):
        return -np.ones_like(allocation)

    def _get_constraints(self, supply, eqtype='eq'):
        result = dict()
        result['type'] = eqtype
        result['fun'] = lambda x: self._constraint(x, supply)
        result['jac'] = self._d_constraint
        return (result)


class LastPeriod(Period):
    def __init__(self, customers, supply, integration_points=10000):
        Period.__init__(self, customers, supply, integration_points)
        self.method = 'brentq'  # was shown to be the best approach.
        self.slsqp = True

    def get_allocation(self, state):
        """
        returns allocations for a given state
        Parameters
        ----------
        state
        a state space object (from framework.optimization)

        Returns
        -------
        the allocations

        """
        if self.supply + state.inventory <= 0:
            return np.zeros(self.dimension)

        vector = state.to_vector()
        if state.inventory not in self.state_list:
            self.state_list[state.inventory] = SortedKeyValueList()

            # TODO: Hier passiert der Fehler --> Beim berechnen der Kosten
            # wiederholt er hier komplett die berechnung, dabei hat er das ergebnis
            # gerade erst vorher berechnet.
        result = self.state_list[state.inventory].get(vector)
        if result is None:
            result, new_state = self._calculate_alloc(state)
            new_vector = new_state.to_vector()
            self.state_list[state.inventory].append(new_vector, result)

        return result

    def get_cost(self, state):
        """
        returns cost for a given state
        Parameters
        ----------
        state
        a state space object

        Returns
        -------
        the total cost (float)

        """

        if state not in self.cost.keys():
            allocation = self.get_allocation(state)
            self.cost[state] = self._calculate_cost(allocation, state)
            return self.cost[state]

        return self.cost[state]

    def _calculate_alloc(self, state):
        if self.dimension == 2:
            allocation = self._calculate_alloc_2_dim(state)
        else:
            if self.slsqp:
                allocation = self._calculate_alloc_slsqp(state)
            else:
                allocation = self._calculate_alloc_n_dim(state)

        # check if allocation is valid for a larger state space
        new_state = deepcopy(state)
        for t_cust in range(self.dimension):
            d_min = cost.minimal_demand(self.customers[t_cust].sl, state.demand[t_cust], state.filled_demand[t_cust])
            if allocation[t_cust] <= d_min:
                # valid for a larger state
                new_state.filled_demand[t_cust] = - (self.customers[t_cust].sl * state.demand[t_cust] - allocation[
                    t_cust] * (1. - self.customers[t_cust].sl))
        return allocation, new_state

    def _calculate_alloc_n_dim(self, state):
        """
        calculates the optimal allocation

        Parameters
        ----------
        state:
            a state space object

        Returns
        -------
        the optimal allocation

        """

        # set available supply and abort if available supply is negative
        available_supply = self.supply + state.inventory

        # initialize the marginal penalty functions and their maximum values


        lambda_max = [-self._d_cost(state, 0., cust_id) for cust_id in range(self.dimension)]

        # calculate a first guess for a minimal lambda:
        lambda_min = 0.

        # define the cumulative inverse of the marginal penalty function:
        def get_allocation(var_lambda):
            allocation = [0.] * self.dimension
            for cust_id in range(self.dimension):
                if var_lambda < lambda_max[cust_id]:
                    if var_lambda == 0.:
                        allocation[cust_id] = self.integration[cust_id].interval_max
                    else:
                        try:
                            result = optimize.brentq(lambda temp_alloc: self._d_cost(state, temp_alloc, cust_id) +
                                                                        var_lambda, 0.,
                                                     available_supply)
                        except ValueError:
                            raise ValueError(
                                "Can not find allocation for lambda={} (max: {}).".format(var_lambda,
                                                                                          lambda_max[cust_id]))

                        allocation[cust_id] = result
            return allocation

        def total_allocation(var_lambda):
            last_allocation = get_allocation(var_lambda)
            return sum(last_allocation)

        try:
            optimal_lambda = optimize.brentq(
                lambda x: total_allocation(x) - available_supply,
                max(lambda_max),
                lambda_min)
        except ValueError:
            raise ValueError("Can not find optimal allocation for supply {}".format(available_supply))

        optimal_allocation = get_allocation(optimal_lambda)
        if abs(sum(optimal_allocation) - available_supply) > 0.1:
            logging.warn("High Error: Allocated {} but {} units available.".format(sum(optimal_allocation),
                                                                                   available_supply))

        return optimal_allocation

    def _d_cost(self, state, x, cust_id):
        return cost.d_expected_cost_last_period(x,
                                                state.demand[cust_id],
                                                state.filled_demand[cust_id],
                                                self.customers[cust_id].sl,
                                                self.customers[cust_id].pen,
                                                self.integration[cust_id])

    def _cost(self, state, x, cust_id):
        return cost.expected_cost_last_period(x,
                                              state.demand[cust_id],
                                              state.filled_demand[cust_id],
                                              self.customers[cust_id].sl,
                                              self.customers[cust_id].pen,
                                              self.integration[cust_id])

    def _calculate_alloc_slsqp(self, state):

        available_supply = self.supply + state.inventory
        x_0 = available_supply * self.mean_demands() / np.sum(self.mean_demands())
        # x_0 = np.zeros(self.dimension)

        res = optimize.minimize(lambda x: sum((self._cost(state, x[i], i) for i in range(self.dimension))),
                                x_0,
                                method='SLSQP',
                                jac=lambda x: [self._d_cost(state, x[i], i) for i in range(self.dimension)],
                                bounds=[(0, available_supply)] * self.dimension,
                                constraints=self._get_constraints(available_supply)
                                )
        return np.array(res.x)

    def _calculate_alloc_2_dim(self, state):
        """
        calculates the optimal allocation for the two customer case.
        The problem then can be reduced to one variable and solved by standard non-linear optimization.


        Parameters
        ----------
        state:
            a state space object

        Returns
        -------
        the optimal allocation

        """

        # set available supply and abort if available supply is negative
        available_supply = self.supply + state.inventory
        if available_supply <= 0:
            return [0, 0]

        # initilize the allocation variables
        alloc = [0, 0]

        # define the marginal cost function
        def d_cost(alloc_0):
            return cost.d_expected_cost_last_period(alloc_0,
                                                    state.demand[0],
                                                    state.filled_demand[0],
                                                    self.customers[0].sl,
                                                    self.customers[0].pen,
                                                    self.integration[0]) - \
                   cost.d_expected_cost_last_period(available_supply - alloc_0,
                                                    state.demand[1],
                                                    state.filled_demand[1],
                                                    self.customers[1].sl,
                                                    self.customers[1].pen,
                                                    self.integration[1])

        # define the second order derivative of the cost
        def dd_cost(alloc_0):
            return cost.dd_expected_cost_last_period(alloc_0, state.demand[0],
                                                     state.filled_demand[0],
                                                     self.customers[0].sl,
                                                     self.customers[0].pen,
                                                     self.customers[0].distribution) + \
                   cost.dd_expected_cost_last_period(available_supply - alloc_0, state.demand[1],
                                                     state.filled_demand[1],
                                                     self.customers[1].sl,
                                                     self.customers[1].pen,
                                                     self.customers[1].distribution)

            # compare the borders for setting the initial x_0

        cost_red_0 = d_cost(0)
        cost_red_1 = d_cost(available_supply)
        # print "at supply {}: d_cost is {:.2f} and {:.2f}".format(available_supply, cost_red_0,cost_red_1)

        if cost_red_0 > 0:  # if the marginal cost of allocating nothing to customer 0
            #  is positive, then allocate nothing to him.
            alloc[0] = 0
        elif cost_red_1 < 0:  # if the marginal cost of allocating everything to customer
            # 0 is positive, allocate everything to him
            alloc[0] = available_supply
        else:
            if self.method == 'auto3':
                start_alloc_1 = (available_supply * cost_red_0) / (
                    cost_red_0 - cost_red_1)  # linear approximation based on d_cost
                next_x = start_alloc_1 - (d_cost(start_alloc_1) / dd_cost(start_alloc_1))
                if 0 < next_x < available_supply:  # try if newton converges
                    alloc[0] = optimize.newton(d_cost, next_x, fprime=dd_cost)
                else:
                    alloc[0] = optimize.brentq(d_cost, 0, available_supply)
            elif self.method == 'brentq':
                alloc[0] = optimize.brentq(d_cost, 0, available_supply)
            elif self.method == 'auto':
                initialization = optimize.brentq(d_cost, 0, available_supply, xtol=available_supply / 20)
                alloc[0] = optimize.newton(d_cost, initialization, fprime=dd_cost)
            elif self.method == 'auto2':
                initialization = optimize.bisect(d_cost, 0, available_supply, xtol=available_supply / 20)
                alloc[0] = optimize.newton(d_cost, initialization, fprime=dd_cost)
            elif self.method == 'newton':
                start_alloc_1 = (available_supply * cost_red_0) / (cost_red_0 - cost_red_1)
                alloc[0] = optimize.newton(d_cost, start_alloc_1, fprime=dd_cost)
            else:
                raise ValueError('Method {} is not supported'.format(self.method))

        alloc[1] = available_supply - alloc[0]

        return alloc

    def _calculate_cost(self, allocation, state):
        total_cost = sum(cost.expected_cost_last_period(allocation[i],
                                                        state.demand[i],
                                                        state.filled_demand[i],
                                                        self.customers[i].sl,
                                                        self.customers[i].pen,
                                                        self.integration[i])
                         for i in range(self.dimension))
        return total_cost

    def reset(self):
        self.input_states = dict()
        self.cost = dict()
        self.state_list = dict()

    def d_cost_allocation(self, state):
        allocation = self.get_allocation(state)
        result = [cost.d_expected_cost_demand(allocation[i],
                                              state.demand[i],
                                              state.filled_demand[i],
                                              self.customers[i].sl,
                                              self.customers[i].pen,
                                              self.integration[i])
                  for i in range(self.dimension)]

        return result


class ApproximatedPeriod(Period):
    def __init__(self, customers, supply, next_period, integration_points=10000):
        Period.__init__(self, customers, supply, integration_points)
        self.next_period = next_period
        self.approach = 'slsqp'

    def get_allocation(self, state):
        available_supply = self.supply + state.inventory
        if available_supply <= 0:
            return np.zeros(self.dimension)
        if self.approach == 'slsqp':
            result = self._get_alloc_slsqp(state, available_supply)
        elif self.approach == 'nlopt':
            result = self._get_alloc_nlopt(state, available_supply)
        elif self.approach == 'nlopt-ccsaq':
            result = self._get_alloc_nlopt_combi(state, available_supply)
        elif self.approach == 'mma':
            result = self._get_alloc_nlopt_mma(state, available_supply)
        elif self.approach == 'ccsaq':
            result = self._get_alloc_nlopt_mma(state, available_supply, 'ccsaq')
        elif self.approach == 'slsqp_red':
            result = self._get_alloc_slsqp_red(state, available_supply)
        elif self.approach == 'cobyla':
            result = self._get_alloc_cobyla(state, available_supply)
        else:
            raise ValueError('Optimization Approach "{}" is not defined'.format(self.approach))
        if np.abs(np.sum(result) - available_supply) > 0.01:
            logging.warn("Optimization Error to high. Used only {} of {}".format(np.sum(result), available_supply))
        return result

    def _get_alloc_slsqp(self, state, available_supply, x_0=None, accuracy=None, depth=None):
        """
        Solves the allocation problem using Scipy.Optimizes SLSQP Implementation

        Apperantly the objecitve function  must be normalized to a a value close to zero.
        Parameters
        ----------
        state : a state space object
            the current state of the system
        available_supply : float_like
            the available supply in the current period

        Returns
        -------
        the alloaction vector (float_like)
        """
        if depth is None:
            depth = 0

        if x_0 is None:
            x_0 = available_supply * self.mean_demands() / np.sum(self.mean_demands())

        if accuracy is None:
            accuracy = 1e-6
        # x_0 = np.zeros(self.dimension) # appers to be slower.
        scale = self._cost_expected_state(x_0, state)

        def objective(x):
            result = self._cost_expected_state(x, state) - scale - 5.
            return result

        def d_objective(x):
            return self._d_cost_expected_state(x, state)

        res = optimize.minimize(objective,
                                x_0,
                                method='SLSQP',
                                jac=d_objective,
                                bounds=[(0, available_supply)] * self.dimension,
                                constraints=self._get_constraints(available_supply),
                                options={'disp': False,
                                         'maxiter': 50,
                                         'ftol': accuracy}
                                )
        if not res.success:
            if res.status == 9:
                logging.warn(res.message)
                self._debug_callback(res.x, state)
            else:
                raise ValueError("Optimization unsucessful.")
        # check allocation quality:
        if np.max(res.jac) - np.min(res.jac) >= 0.1:
            self._debug_callback(res.x, state)
            logging.warn("Optimization inaccurate {}/{}".format(res.fun, res.jac))
            if depth < 1:
                logging.warn("Trying Cobyla")
                new_x = self._get_alloc_cobyla(state, available_supply, res.x)
                jac = d_objective(new_x)
                if np.max(jac) - np.min(jac) >= 0.1:
                    logging.warn("Cobyla inaccurate {}".format(jac))
                else:
                    logging.warn("Coblya succesful!")

        logging.info("Solver finished with {} Iterations".format(res.nit))
        return np.array(res.x)

    def _get_alloc_nlopt(self, state, available_supply):

        def objective(x, grad):
            if grad.size > 0:
                grad[:] = self._d_cost_expected_state(x, state)[:]
            result = self._cost_expected_state(x, state)
            return result

        def constraint(x, grad):
            if grad.size > 0:
                grad[:] = self._d_constraint(x)[:]
            result = self._constraint(x, available_supply)
            return result

        # build nlopt optimization
        opt = nlopt.opt(nlopt.LD_SLSQP, self.dimension)
        opt.set_min_objective(objective)
        opt.set_lower_bounds(np.zeros(self.dimension))
        opt.set_upper_bounds(np.ones(self.dimension) * available_supply)
        opt.add_equality_constraint(constraint)

        opt.set_maxtime(3)  # in seconds
        tol = 1e-3
        x_0 = np.ones(self.dimension) * available_supply / self.dimension
        counter = 0
        while 1:
            opt.set_xtol_abs(tol)
            x_opt = opt.optimize(x_0)
            counter += 1
            termination = opt.last_optimize_result()
            if termination == 6:
                logging.warn("Optimization hit time limit.")
            elif termination != 4:
                logging.warn("Unknown Termination Code {} for nlopt".format(termination))
            gradient = self._d_cost_expected_state(x_opt, state)
            if self._optimality(x_opt, gradient):
                if counter > 1:
                    logging.warn("Found Solution: {}, x={}.".format(gradient, x_opt))
                if abs(sum(x_opt) - available_supply) > 0.01:
                    logging.warn("Solution didn't use all supply: {}, x={}.".format(gradient, x_opt))
                break
            elif counter <= 3:
                logging.warn("Gradient suggests non-optimality: {}, x={}, trying again.".format(gradient, x_opt))
                x_0 = x_opt
                tol = tol * 1e-3
            else:
                logging.warn("Gradient suggests non-optimality: {}, x={}, aborted.".format(gradient, x_opt))
                break

        return x_opt

    def _get_alloc_nlopt_combi(self, state, available_supply):
        last_x = None
        def objective(x, grad):
            global last_x
            last_x = x
            if grad.size > 0:
                grad[:] = self._d_cost_expected_state(x, state)[:]
            objective_value = self._cost_expected_state(x, state)
            return objective_value

        def constraint(x, grad):
            if grad.size > 0:
                grad[:] = -self._d_constraint(x)[:]
            result = -self._constraint(x, available_supply)
            return result

        # build nlopt optimization
        opt = nlopt.opt(nlopt.LD_SLSQP, self.dimension)
        opt.set_min_objective(objective)
        opt.set_lower_bounds(np.zeros(self.dimension))
        opt.set_upper_bounds(np.ones(self.dimension) * available_supply)
        opt.add_equality_constraint(constraint)

        opt.set_maxtime(3)  # in seconds
        tol = 1e-5
        x_0 = np.ones(self.dimension) * available_supply / self.dimension

        opt.set_xtol_abs(tol)
        try:
            x_opt = opt.optimize(x_0)
        except nlopt.RoundoffLimited:
            #reduce tolerance
            logging.warn("Catched RoundoffLimited Error. Only use LD_CCSAQ.")
            x_opt = x_0
        else:
            termination = opt.last_optimize_result()
            if termination == 6:
                logging.debug("Optimization hit time limit.")
            elif termination != 4:
                logging.warn("Unknown Termination Code {} for nlopt".format(termination))
        gradient = self._d_cost_expected_state(x_opt, state)
        if not self._optimality(x_opt, gradient):
            logging.debug("Gradient suggests non-optimality: {}, x={}, Using LD_CCSAQ.".format(gradient, x_opt))
            opt = nlopt.opt(nlopt.LD_CCSAQ, self.dimension)
            opt.set_min_objective(objective)
            opt.set_lower_bounds(np.zeros(self.dimension))
            opt.set_upper_bounds(np.ones(self.dimension) * available_supply)
            opt.add_inequality_constraint(constraint)
            opt.set_maxtime(3)
            opt.set_xtol_abs(tol)
            x_opt = opt.optimize(x_opt)
            gradient = self._d_cost_expected_state(x_opt, state)
            if not self._optimality(x_opt, gradient):
                logging.warn("Gradient suggests non-optimality: {}, x={}, LD_CCSAQ failed.".format(gradient, x_opt))

        return x_opt


    def _get_alloc_nlopt_mma(self, state, available_supply, mode = 'mma'):

        def objective(x, grad):
            if grad.size > 0:
                grad[:] = self._d_cost_expected_state(x, state)[:]
            result = self._cost_expected_state(x, state)
            return result

        def constraint(x, grad):
            if grad.size > 0:
                grad[:] = -self._d_constraint(x)[:]
            result = -self._constraint(x, available_supply)
            return result

        # build nlopt optimization
        if mode == 'ccsaq':
            opt = nlopt.opt(nlopt.LD_CCSAQ, self.dimension)
        else:
            opt = nlopt.opt(nlopt.LD_MMA, self.dimension)

        opt.set_min_objective(objective)
        opt.set_lower_bounds(np.zeros(self.dimension))
        opt.set_upper_bounds(np.ones(self.dimension) * available_supply)
        opt.add_inequality_constraint(constraint)

        opt.set_maxtime(3)  # in seconds
        tol = 1e-5
        x_0 = np.ones(self.dimension) * available_supply / self.dimension
        counter = 0
        while 1:
            opt.set_xtol_abs(tol)
            x_opt = opt.optimize(x_0)
            counter += 1
            termination = opt.last_optimize_result()
            if termination == 6:
                logging.warn("Optimization hit time limit.")
            elif termination != 4:
                logging.warn("Unknown Termination Code {} for nlopt".format(termination))
            grad = self._d_cost_expected_state(x_opt, state)
            if self._optimality(x_opt, grad):
                if counter > 1:
                    logging.warn("Found Solution: {}, x={}.".format(grad, x_opt))
                break
            elif counter <= 3:
                logging.warn("Gradient suggests non-optimality: {}, x={}, trying again.".format(grad, x_opt))
                x_0 = x_opt
                tol = tol * 1e-3
            else:
                logging.warn("Gradient suggests non-optimality: {}, x={}, aborted.".format(grad, x_opt))
                break

        return x_opt


    def _get_alloc_slsqp_red(self, state, available_supply):

        x_0 = available_supply * self.mean_demands() / np.sum(self.mean_demands())

        scale = float(self._cost_expected_state(x_0, state))
        print scale

        # x_0 = np.zeros(self.dimension)

        def full_x(x_red):
            x = np.zeros(self.dimension)
            x[1:] = x_red
            x[0] = np.maximum(available_supply - np.sum(x_red), 0.)
            return x

        constraint_dict = dict()
        constraint_dict['type'] = 'ineq'
        constraint_dict['fun'] = lambda x: available_supply + np.sum(x)
        constraint_dict['jac'] = lambda x: - np.ones(self.dimension - 1, dtype=np.float)

        def objective(x_red):
            return float(self._cost_expected_state(full_x(x_red), state) + 5000.)

        def d_objective(x_red):
            dfull = self._d_cost_expected_state(full_x(x_red), state)
            result = np.zeros(self.dimension - 1, dtype=np.float)
            result = dfull[1:] - dfull[0]
            return [float(t_result) for t_result in result]

        def print_callback(x):
            print "Current x0={}".format(hlp.array_to_str(full_x(x)))
            print "Gradient  ={}".format(hlp.array_to_str(d_objective(x)))
            print "Obj-Value ={}".format(objective(x))
            print "Constraint={}".format(constraint_dict['fun'](x))

        print_callback(x_0[1:])
        res = optimize.minimize(objective,
                                x_0[1:],
                                method='SLSQP',
                                # jac=d_objective,
                                bounds=[(0., available_supply)] * (self.dimension - 1),
                                constraints=(constraint_dict),
                                options={'disp': True,
                                         'maxiter': 200,
                                         'ftol': 1e-5,
                                         'iprint': 10},
                                callback=print_callback
                                )
        if not res.success:
            logging.error("Optimization Status: {}".format(res))
            raise ValueError("Optimization unsucessful.")
        return np.array(full_x(res.x))

    def _get_alloc_cobyla(self, state, available_supply, x_0=None):
        if x_0 is None:
            x_0 = np.zeros(self.dimension)
        res = optimize.minimize(self._cost_expected_state,
                                x_0,
                                args=(state),
                                method='cobyla',
                                bounds=[(0, available_supply)] * self.dimension,
                                constraints=self._get_constraints(available_supply, eqtype='ineq')
                                )
        if not res.success:
            if res.mode == 9:
                logging.warn("Iteration limit exceedet")
            else:
                raise ValueError("Optimization unsucessful.")
        return np.array(res.x)

    def _cost_expected_state(self, allocation_vector, state):
        next_state = self.next_state(state, allocation_vector)
        ecost = self.next_period.get_cost(next_state)
        # logging.debug("Cost {} ({})".format(ecost, allocation_vector))
        return float(ecost)

    def _d_cost_expected_state(self, allocation_vector, state):

        if any(np.isnan(allocation_vector)):
            raise ValueError("Allocation may not be nan")
        next_state = self.next_state(state, allocation_vector)
        d_cost = self.next_period.d_cost_allocation(next_state)
        consumption_probability = np.array(
            [(1. - self.customers[i].distribution.cdf(allocation_vector[i])) for i in
             range(self.dimension)])
        d_cost = d_cost * consumption_probability
        return d_cost

    def get_cost(self, state):
        pass

    def _debug_callback(self, allocation_vector, state):
        logging.debug("Current x0={}\n".format(hlp.array_to_str(allocation_vector)) +
                      "Gradient  ={}".format(
                          hlp.array_to_str(self._d_cost_expected_state(allocation_vector, state))) +
                      "Obj-Value ={}".format(self._cost_expected_state(allocation_vector, state)))

    @staticmethod
    def _optimality(x, grad):
        positive_allocation = x > 1e-5
        if np.max(grad[positive_allocation]) - np.min(grad[positive_allocation]) >= 5e-2:
            return False
        else:
            return True
