# -*- coding: utf-8 -*-

def _d_cost(demand, integration, start, penalty):
    """
    first order derivative of the  cost function
    Parameters
    ----------
    demand
    penalty
    integration
    start

    Returns
    -------

    """

    def func(x):
        return 1. / (demand + x)

    result = - penalty * integration.integrate(func, start)

    return result


def _dd_cost(allocation, demand, penalty, distribution, start):
    """
    second order derivative of the cost function
    Parameters
    ----------
    allocation
    demand
    penalty
    distribution
    start

    Returns
    -------

    """
    if allocation + demand == 0:
        return 0
    else:
        return penalty * distribution.pdf(start) / (allocation + demand)


def minimal_demand(sl_target, demand, fulfilled_demand):
    """
    The minimal demand leading to a sufficient service level.
    Parameters
    ----------
    sl_target
    demand
    fulfilled_demand

    Returns
    -------

    """

    return (sl_target * demand - fulfilled_demand) / (1. - sl_target)


def maximal_demand(allocation, sl_target, demand, fulfilled_demand):
    """
    The maximal demand that will lead to a sufficient service level.
    Parameters
    ----------
    allocation
    sl_target
    demand
    fulfilled_demand

    Returns
    -------

    """
    return (fulfilled_demand + allocation) / sl_target - demand


def calc_sl(demand, fulfilled_demand):
    """
    Calculates the fill rate/beta service level
    Parameters
    ----------
    demand
    fulfilled_demand

    Returns
    -------
    fill rate
    """
    if demand == 0:
        return 1.
    else:
        return fulfilled_demand / float(demand)


def expected_cost_last_period(allocation, demand, fulfilled_demand, sl_target, penalty, integration):
    """
    Calculates the expected penalty for a given allocation

    Parameters
    ----------
    allocation
    demand
    fulfilled_demand
    sl_target
    penalty
    integration:
        NumericIntegration Object

    Returns
    -------

    """
    d_min = minimal_demand(sl_target, demand, fulfilled_demand)
    d_max = maximal_demand(allocation, sl_target, demand, fulfilled_demand)
    current_sl = calc_sl(demand, fulfilled_demand)
    if current_sl >= sl_target:
        cost = penalty * integration.integrate(
            lambda xi: (sl_target - (fulfilled_demand + allocation) / (demand + xi)),
            start=d_max)
    elif allocation > d_min:
        cost = penalty * integration.integrate(
            lambda xi: sl_target - (fulfilled_demand + xi) / (demand + xi),
            end=d_min) + \
               penalty * integration.integrate(
                   lambda xi: sl_target - (fulfilled_demand + allocation) / (demand + xi),
                   start=d_max
               )
    else:
        cost = penalty * integration.integrate(
            lambda xi: sl_target - (fulfilled_demand + xi) / (demand + xi),
            end=allocation) + \
               penalty * integration.integrate(
                   lambda xi: sl_target - (fulfilled_demand + allocation) / (demand + xi),
                   start=allocation
               )
    return cost


def d_expected_cost_last_period(allocation, demand, fulfilled_demand, sl_target, penalty, integration):
    """
     Calculates the first order derivative of the expected cost in period T
    Parameters
    ----------
    allocation
    demand
    fulfilled_demand
    sl_target
    penalty
    integration
        a integration object

    Returns
    -------

    """
    d_min = minimal_demand(sl_target, demand, fulfilled_demand)
    d_max = maximal_demand(allocation, sl_target, demand, fulfilled_demand)

    if allocation >= d_min:
        return _d_cost(demand, integration, d_max, penalty)

    else:
        return _d_cost(demand, integration, allocation, penalty)


def dd_expected_cost_last_period(allocation, demand, fulfilled_demand, sl_target, penalty, distribution):
    """
    Calculates the second order derivative of the expected cost in period T
    Parameters
    ----------
    allocation
    demand
    fulfilled_demand
    sl_target
    penalty
    distribution
        a scipy distribution object

    Returns
    -------

    """
    d_min = minimal_demand(sl_target, demand, fulfilled_demand)
    d_max = maximal_demand(allocation, sl_target, demand, fulfilled_demand)

    if allocation > d_min:
        return _dd_cost(allocation, demand, penalty, distribution, d_max)
    else:
        return _dd_cost(allocation, demand, penalty, distribution, allocation)


def d_expected_cost_demand(allocation, demand, fulfilled_demand, sl_target, penalty, integration):
    """
    Calculates the marginal penalty with respect to fulfilled demand
    Parameters
    ----------
    allocation
    demand
    fulfilled_demand
    sl_target
    penalty
    integration

    Returns
    -------

    """
    d_max = maximal_demand(allocation, sl_target, demand, fulfilled_demand)
    d_min = minimal_demand(sl_target, demand, fulfilled_demand)
    sl = calc_sl(demand, fulfilled_demand)
    if sl >= sl_target:
        result = -penalty * integration.integrate(
            lambda xi: 1. / (demand + xi),
            start=d_max
        )
    elif allocation >= d_min:
        result = -penalty * integration.integrate(
            lambda xi: 1. / (demand + xi),
            end=d_min
        ) - penalty * integration.integrate(
            lambda xi: 1. / (demand + xi),
            start=d_max
        )
    else:
        result = - penalty * integration.integrate(
            lambda xi: 1 / (demand + xi))
    return result
