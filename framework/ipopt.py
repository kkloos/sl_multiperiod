import pyipopt


def optimize_pyomo():
    nlp = pyipopt.create(nvar,  # number of variables
                         x_L,  # lower bound
                         x_U,   # upper bound
                         ncon,  # number of constraints
                         g_L,   # lower bound for the constraint??
                         g_U,   # upper bound for the constraint??
                         nnzj,  #impr
                         nnzh,
                         eval_f,
                         eval_grad_f,
                         eval_g,
                         eval_jac_g)
imNL