import framework.objects as opt
import numpy as np
import period
from copy import deepcopy
import framework.utils.helper as hlp
import time
import pandas as pd
import logging


def build_customers(s):
    """
    Builds the customer objects for the heuristic
    Parameters
    ----------
    s the setup object

    Returns
    -------
    period_customers : a list of lists of customers for each period
    aggregated_customers : a list of lists of the aggregated customers for upcoming periods
    """
    period_customers = list()
    aggregated_customers = list()
    for t in range(s.t):
        period_customers.append(list())
        aggregated_customers.append(list())
        for n in range(s.n):
            period_customers[-1].append(
                opt.Customer(s.sl[n],
                             s.pen[n],
                             s.demand[n, t],
                             s.std[n, t])
            )
            if not t == s.t - 1:
                aggregated_customers[-1].append(
                    opt.Customer(s.sl[n],
                                 s.pen[n],
                                 np.sum(s.demand[n, t + 1:]),
                                 np.sqrt(np.sum(np.power(s.std[n, t + 1:], 2))))
                )

    return period_customers, aggregated_customers


def effective_supply(ip, e_ip, supply, start_period):
    """the effective supply that is available form start period to end of horizon
    """
    corrected_ip = e_ip + ip
    s_period = supply[start_period:] + corrected_ip[start_period - 1:-1]
    s_period = np.maximum(s_period, 0)
    result = np.sum(s_period)
    return result


def run_heuristic(s, demands, start=0, mode='slsqp'):
    """
    calculates the heuristic allocations and resulting penalty
    Parameters
    ----------
    s: Setup-Object
        The setup of the experiment
    demands: np-array
        the demand realizations (dimensions n x t x realizations)
    start : experiment to start from

    Returns
    -------
    pd_results : pandas Dataframe
        the Penalty and Runtime
    allocations : np-array
        the allocation for each period
    """
    allocations = np.zeros(demands.shape)
    pd_results = pd.DataFrame()
    customers_1p, customers_np = build_customers(s)

    e_ip = s.supply - np.sum(s.demand, axis=0)  # calculate the expected change in the inventory position

    # calculate the allocation for the first period:
    next_period = period.LastPeriod(customers_np[0], effective_supply(0., e_ip, s.supply, 1))
    first_period = period.ApproximatedPeriod(customers_1p[0], s.supply[0], next_period)
    start_state = opt.StateSpace(np.zeros(s.n), np.zeros(s.n), 0)
    first_period.approach = mode
    fp_alloc = first_period.get_allocation(start_state)
    allocations[:, 0, :] = np.tile(fp_alloc, (demands.shape[2], 1)).T

    # the last period allocation problem is always identical
    last_period = period.LastPeriod(customers_1p[s.t - 1], s.supply[s.t - 1])

    for t_ex in range(start, demands.shape[2]):
        start = time.time()
        this_state = deepcopy(start_state)
        logging.info("\nExperiment {}\n------------------".format(t_ex))
        available_supply = np.maximum(hlp.supply_netting(s.supply, demands[:, :, t_ex]), 0)
        for t in range(1, s.t - 1):
            logging.debug("Period {}".format(t))
            this_state.update(demands[:, t - 1, t_ex], allocations[:, t - 1, t_ex], s.supply[t-1])
            this_supply = this_state.inventory+s.supply[t]
            if np.abs(np.maximum(this_supply, 0) - available_supply[t]) >= 1e-4:
                raise ValueError('Supply not correct (is {}, but should be {}'.format(this_supply, available_supply[t]))
            logging.debug("State: \n{}".format(this_state))
            remaining_supply = effective_supply(this_state.inventory, e_ip, s.supply, t + 1)
            logging.debug("Effective Supply: {:.2f}".format(remaining_supply))
            logging.debug("Next Customers:\n{}".format(customers_np[t]))
            next_period = period.LastPeriod(customers_np[t],
                                            remaining_supply)
            this_period = period.ApproximatedPeriod(customers_1p[t], s.supply[t], next_period)
            this_period.approach = mode
            allocations[:, t, t_ex] = this_period.get_allocation(this_state)
            logging.debug("allocation/supply :{:.2f}/{:.2f}".format(np.sum(allocations[:, t, t_ex]),
                          s.supply[t] + this_state.inventory))
            logging.debug("allocation: {}".format(hlp.array_to_str(allocations[:, t, t_ex])))
            logging.debug("demands   : {}".format(hlp.array_to_str(demands[:, t, t_ex])))

        # get the allocation for the last period
        this_state.update(demands[:, s.t - 2, t_ex], allocations[:, s.t - 2, t_ex], s.supply[s.t-2])
        allocations[:, s.t - 1, t_ex] = last_period.get_allocation(this_state)

        # calculate penalties
        penalty = hlp.calculate_penalty(s.sl, s.pen, demands[:, :, t_ex], allocations[:, :, t_ex])
        runtime = time.time() - start
        this_result = pd.DataFrame([[penalty, runtime]], columns=['Penalty', 'Runtime'], index=[t_ex])
        pd_results = pd_results.append(this_result)

        pd_results['Method'] = 'heuristic-{}'.format(mode)
    return pd_results, allocations
