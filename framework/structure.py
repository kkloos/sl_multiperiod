# -*- coding: utf-8 -*-
class Setup(object):
    # TODO: Modify this object such that is checks the data types for consistency.
    # check that when setting the parameters that the dimension (t and n in the description) fit
    # also check that every entry is non-negative and make the numpy conversion

    def __init__(self, ident, t=None, n=None, demand=None, std=None, supply=None, sl=None, pen=None):
        self.id = ident  # int
        self.t = t  # int / number of periods
        self.n = n  # int / number of customers
        self.demand = demand  # np-array with n x t
        self.std = std  # np-array with n x t
        self.supply = supply  # np-array with n
        self.sl = sl  # service-level : np-array with n 0<sl<1
        self.pen = pen  # np-array with n

    @property
    def id(self):
        return self._id
    
    @id.setter
    def id(self, value):
        self._id = int(value)

    @property
    def t(self):
        return self._t

    @t.setter
    def t(self, value):
        if not value is None:
            self._t = int(value)
        else:
             self._t = value

    @property
    def n(self):
        return self._n

    @n.setter
    def n(self, value):
        if not value is None:
            self._n = int(value)
        else:
             self._n = value

    @property
    def demand(self):
        return self._demand

    @demand.setter
    def demand(self, value):
        self._demand = value

    @property
    def std(self):
        return self._std

    @std.setter
    def std(self, value):
        self._std = value

    @property
    def supply(self):
        return self._supply

    @supply.setter
    def supply(self, value):
        self._supply = value

    @property
    def sl(self):
        return self._sl

    @sl.setter
    def sl(self, value):
        self._sl = value

    @property
    def pen(self):
        return self._pen

    @pen.setter
    def pen(self, value):
        self._pen = value

    @classmethod
    def from_dict(cls, dictionary):
        myid = dictionary.pop('_id')
        me = cls(myid)
        me.__dict__.update(dictionary)
        return me

    def __repr__(self):
        return repr(vars(self))

