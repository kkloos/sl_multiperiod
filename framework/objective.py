# -*- coding: utf-8 -*-
"""
Created on Wed Aug 02 08:28:15 2017

@author: kok29wv
"""

import numpy as np
import scipy.integrate as scint


def calc_sl(demand, fulfilled_demand):
    """
    Calculates the fill rate/beta service level
    Parameters
    ----------
    demand
    fulfilled_demand

    Returns
    -------
    fill rate
    """
    if demand == 0:
        return 1.
    else:
        return fulfilled_demand / float(demand)


def minimal_demand(sl_target, demand, fulfilled_demand):
    """
    The minimal demand leading to a sufficient service level.
    Parameters
    ----------
    sl_target
    demand
    fulfilled_demand

    Returns
    -------

    """

    return (sl_target * demand - fulfilled_demand) / (1. - sl_target)


def maximal_demand(allocation, sl_target, demand, fulfilled_demand):
    """
    The maximal demand that will lead to a sufficient service level.
    Parameters
    ----------
    allocation
    sl_target
    demand
    fulfilled_demand

    Returns
    -------

    """
    return (fulfilled_demand + allocation) / sl_target - demand


def _cost_part_unsufficient(allocation, demand, fulfilled_demand, sl_target, penalty, distribution, start):
    """
    Expected cost when allocation is not sufficient
    Parameters
    ----------
    allocation
    demand
    fulfilled_demand
    sl_target
    penalty
    distribution
    start

    Returns
    -------

    """

    def cost_func(xi):
        return (sl_target - (fulfilled_demand + allocation) / (demand + xi)) * distribution.pdf(xi)

    integration = scint.quad(cost_func, start, np.infty)
    return penalty * integration[0]


def _cost_part_sufficient(demand, fulfilled_demand, sl_target, penalty, distribution, start, end):
    """
    Expected cost if allocation is sufficient
    Parameters
    ----------
    demand
    fulfilled_demand
    sl_target
    penalty
    distribution
    start
    end

    Returns
    -------

    """

    def cost_func(xi):
        return (sl_target - (fulfilled_demand + xi) / (demand + xi)) * distribution.pdf(xi)

    integration = scint.quad(cost_func, start, end)
    return penalty * integration[0]


def _d_cost(demand, penalty, distribution, start):
    """
    first order derivative of the  cost function
    Parameters
    ----------
    demand
    penalty
    distribution
    start

    Returns
    -------

    """

    def cost_func(xi):
        return distribution.pdf(xi) / (demand + xi)

    integration = scint.quad(cost_func, start, np.infty)
    return -penalty * integration[0]


def _dd_cost(allocation, demand, penalty, distribution, start):
    """
    second order derivative of the cost function
    Parameters
    ----------
    allocation
    demand
    penalty
    distribution
    start

    Returns
    -------

    """
    if allocation + demand == 0:
        return 0
    else:
        return penalty * distribution.pdf(start) / (allocation + demand)


@np.vectorize
def expected_cost_last_period(allocation, demand, fulfilled_demand, sl_target, penalty, distribution):
    """
     Calculates the expected cost in period T
    Parameters
    ----------
    allocation
    demand
    fulfilled_demand
    sl_target
    penalty
    distribution
        a scipy distribution object

    Returns
    -------

    """

    d_min = minimal_demand(sl_target, demand, fulfilled_demand)
    d_max = maximal_demand(allocation, sl_target, demand, fulfilled_demand)

    cur_sl = calc_sl(demand, fulfilled_demand)

    if cur_sl >= sl_target:
        return _cost_part_unsufficient(allocation, demand, fulfilled_demand, sl_target, penalty, distribution, d_max)
    else:
        if allocation > d_min:
            return _cost_part_sufficient(demand, fulfilled_demand, sl_target, penalty, distribution, 0,
                                         d_min) + \
                   _cost_part_unsufficient(allocation, demand, fulfilled_demand,
                                           sl_target, penalty, distribution, d_max)
        else:
            return _cost_part_sufficient(demand, fulfilled_demand, sl_target, penalty, distribution, 0,
                                         allocation) + \
                   _cost_part_unsufficient(allocation, demand, fulfilled_demand,
                                           sl_target, penalty, distribution,
                                           allocation)


@np.vectorize
def d_expected_cost_last_period(allocation, demand, fulfilled_demand, sl_target, penalty, distribution):
    """
     Calculates the first order derivative of the expected cost in period T
    Parameters
    ----------
    allocation
    demand
    fulfilled_demand
    sl_target
    penalty
    distribution
        a scipy distribution object

    Returns
    -------

    """
    d_min = minimal_demand(sl_target, demand, fulfilled_demand)
    d_max = maximal_demand(allocation, sl_target, demand, fulfilled_demand)

    cur_sl = calc_sl(demand, fulfilled_demand)

    if cur_sl >= sl_target:
        return _d_cost(demand, penalty, distribution, d_max)
    else:
        if allocation > d_min:
            return _d_cost(demand, penalty, distribution, d_max)
        else:
            return _d_cost(demand, penalty, distribution, allocation)


@np.vectorize
def dd_expected_cost_last_period(allocation, demand, fulfilled_demand, sl_target, penalty, distribution):
    """
    Calculates the second order derivative of the expected cost in period T
    Parameters
    ----------
    allocation
    demand
    fulfilled_demand
    sl_target
    penalty
    distribution
        a scipy distribution object

    Returns
    -------

    """
    d_min = minimal_demand(sl_target, demand, fulfilled_demand)
    d_max = maximal_demand(allocation, sl_target, demand, fulfilled_demand)

    if allocation > d_min:
        return _dd_cost(allocation, demand, penalty, distribution, d_max)
    else:
        return _dd_cost(allocation, demand, penalty, distribution, allocation)


def ob_cost(allocation, customer_id, statespace, customer):
    d, f_d = statespace.get_demands(customer_id)
    return expected_cost_last_period(allocation, d, f_d, customer.sl, customer.pen, customer.distribution)


def ob_dcost(allocation, customer_id, statespace, customer):
    d, f_d = statespace.get_demands(customer_id)
    return d_expected_cost_last_period(allocation, d, f_d, customer.sl, customer.pen, customer.distribution)


def ob_ddcost(allocation, customer_id, statespace, customer):
    d, f_d = statespace.get_demands(customer_id)
    return dd_expected_cost_last_period(allocation, d, f_d, customer.sl, customer.pen, customer.distribution)
