from gurobipy import *
import numpy as np
from framework.utils.helper import supply_netting


def solve_model(demand, supply, service_levels, penalties, ip=None, x_0=None, y_0=None, output=False):
    """Solves the deterministic LP-Model.

    Parameters
    ----------
    demand : np-array
        demand matrix where dim=0 is the customer dimension and dim=1 is the time dimension
    supply : np-array
        supply vector
    service_levels : np-array
        vector of the service level targets
    penalties : np-array
        vector of the penalties
    ip : float
        inventory position
    x_0 : np-array
        demand that has already occured
    y_0 : np-array
        demand that has already been fulfilled

    Returns
    -------

    """
    # get problem dimensions
    n_customers = int(demand.shape[0])
    n_period = int(demand.shape[1])
    if ip is None:
        ip = 0.

    if x_0 is None:
        x_0 = np.zeros(n_customers)

    if y_0 is None:
        y_0 = np.zeros(n_customers)

    if len(supply) != n_period:
        raise ValueError('number of periods is not consistent')

    if len(service_levels) != n_customers or len(penalties) != n_customers or len(x_0) != n_customers or len(y_0) != \
            n_customers:
        raise ValueError('number of customers is not consistent')

    # supply netting
    available_supply = supply_netting(supply, demand, ip)

    # build the model

    m = Model("Allocation Planning")
    if not output:
        m.setParam(GRB.Param.OutputFlag, 0)
    # create the variables

    alloc = m.addVars(n_customers, n_period, name='allocation', lb=0, ub=GRB.INFINITY, vtype=GRB.CONTINUOUS)
    cost = m.addVars(n_customers, name='cost', lb=0, ub=GRB.INFINITY, vtype=GRB.CONTINUOUS)

    m.update()

    # build objective
    m.setObjective(quicksum(cost[i] for i in range(n_customers)), GRB.MINIMIZE)

    # add cost constraints

    c_cost = m.addConstrs((
        cost[i] >= penalties[i] * (service_levels[i] - (y_0[i] + quicksum(alloc[i, t] for t in range(n_period))) /
                                   (x_0[i] + np.sum(demand[i, :])))
        for i in range(n_customers)),
        name='Cost_Constraint')

    c_alloc = list()
    for t in range(n_period):
        if available_supply[t] > 0:
            c_alloc.append(m.addConstr(quicksum(alloc[i, t] for i in range(n_customers)) <= available_supply[t],
                                       name='Supply_Constraint_{}'.format(t)))
        else:
            c_alloc.append(m.addConstrs((alloc[i, t] == 0 for i in range(n_customers)),
                                        name='Supply_Constraint_{}'.format(t)))

    c_demand = m.addConstrs((alloc[i, t] <= demand[i, t] for t in range(n_period) for i in range(n_customers)),
                            'Demand_Constraint')

    # solve

    m.optimize()

    if m.Status != GRB.OPTIMAL:
        raise Exception("Gurobi did not find an optimal solution. Status {}".format(m.Status))

    # build allocation matrix:
    allocations = np.zeros_like(demand)
    for index in alloc.iterkeys():
        allocations[index] = alloc[index].X

    # unallocated supply
    unallocated = np.zeros(n_period)
    for t in range(n_period):
        if available_supply[t] > 0:
            unallocated[t] = c_alloc[t].Slack

    return m, allocations, unallocated


if __name__ == '__main__':
    periods = 5
    customers = 3
    supply = [30] * periods
    demand = np.maximum(np.random.normal(10, 3, (customers, periods)), 0)
    sl = [0.9, 0.99, 0.95]
    pen = [1000, 1500, 3000]

    opt, alloc, free = solve_model(demand, supply, sl, pen, ip=0.)

    print "Objective: {}".format(opt.ObjVal)
    print "Demand:"
    print np.sum(demand, axis=0)
    print "Supply:"
    print supply
    print "Allocation:"
    print np.sum(alloc, axis=0)
    print "Period-SL:"
    print alloc / demand * 100.
    print "Unallocated"
    print free
