# -*- coding: utf-8 -*-
import framework.utils.helper as hlp
import numpy as np
import time
import pandas as pd


def get_allocation(available_supply, mean_demand):
    """
    calculates a per commit allocation for a given demand realization
    Parameters
    ----------
    demand
    supply
    mean_demand

    Returns
    -------
    the allocation (np-array)
    """

    factors = np.divide(mean_demand, np.sum(mean_demand, axis=0), dtype=np.float)
    allocations = available_supply * factors
    return allocations


def run_per_commit(s, demands):
    """
    calculates the per commit allocations and penalty
    Parameters
    ----------
    s: Setup-Object
        The setup of the experiment
    demands: np-array
        the demand realizations (dimensions n x t x realizations)

    Returns
    -------
    pd_results : pandas Dataframe
        the Penalty and Runtime
    allocations : np-array
        the allocation for each period
    """
    experiments = demands.shape[2]
    allocation = np.zeros(demands.shape)
    pd_results = pd.DataFrame()
    for t_ex in range(experiments):
        start = time.time()
        available_supply = np.maximum(hlp.supply_netting(s.supply, demands[:, :, t_ex]), 0)
        t_alloc = get_allocation(available_supply, s.demand)
        allocation[:, :, t_ex] = t_alloc
        penalty = hlp.calculate_penalty(s.sl, s.pen, demands[:, :, t_ex], t_alloc)
        runtime = time.time() - start
        this_result = pd.DataFrame([[penalty, runtime]], columns=['Penalty', 'Runtime'], index=[t_ex])
        pd_results = pd_results.append(this_result)

    pd_results['Method'] = 'percommit'
    return pd_results, allocation


if __name__ == '__main__':
    demand = np.array([[10, 20], [20, 10]])
    supply = [20, 40]
    mean_demand = np.array([[10, 10], [10, 10]])
    print get_allocation(demand, supply, mean_demand)
