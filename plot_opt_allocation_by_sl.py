# -*- coding: utf-8 -*-
"""
Created on Wed Aug 02 08:27:26 2017

@author: kok29wv
"""

import framework.objects as opt
import numpy as np
import matplotlib.pyplot as plt
# import seaborn as sns
import framework.period
import pandas as pd
import time

# define customers

customers = list()
customers.append(opt.Customer(0.9, 1000, 100, 8))
customers.append(opt.Customer(0.9, 1000, 100, 8))

min_sl = 0
max_sl = 1.
n_sl = 50

demand_a = 10.

sl_0 = np.linspace(min_sl, max_sl, n_sl)
sl_1 = np.linspace(min_sl, max_sl, n_sl)

results = pd.DataFrame()
speed = pd.DataFrame()
lperiod = framework.period.LastPeriod(customers, 15)
for sl_0_id, t_sl_0 in enumerate(sl_0):
    # print "{} %".format((sl_0_id+0.)/n_sl*100.)
    for sl_1_id, t_sl_1 in enumerate(sl_1):
        ss = opt.StateSpace([demand_a, 10.], [t_sl_0 * demand_a, t_sl_1 * 10.], 0.)
        res = lperiod.get_allocation(ss)
        cost = lperiod.get_cost(ss)
        d_alloc = lperiod.d_cost_allocation(ss)
        results = results.append(pd.DataFrame([[t_sl_0, t_sl_1] + res + [cost]+d_alloc], columns=['sl A', 'sl B', 'alloc A',
                                                                                          'alloc B',
                                                                                          'cost',
                                                                                          'd y A',
                                                                                          'd y B'
                                                                                          ],
                                              index=[(sl_0_id, sl_1_id)]))

# create an array:
        
for t_data in ['d y A', 'd y B', 'cost', 'alloc B']:
    alloc_array = np.zeros((n_sl, n_sl))
    for index, series in results.iterrows():
        alloc_array[index] = series[t_data]
    
    plt.figure(figsize=(6, 5))
    plt.pcolormesh(sl_0, sl_1, alloc_array)
    plt.xlim(min_sl, max_sl)
    plt.ylim(min_sl, max_sl)
    plt.title(t_data)
    plt.colorbar()
    plt.tight_layout()
