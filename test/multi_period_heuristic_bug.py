# -*- coding: utf-8 -*-

import framework.objects as opt
import numpy as np
import matplotlib.pyplot as plt
# import seaborn as sns
import framework.period
import pandas as pd
import time
import logging

logger = logging.getLogger()
logger.disabled = False
logger.level = logging.DEBUG

# define customers

customers_0 = list()
customers_0.append(opt.Customer(0.8, 1000, 10, 3))
customers_0.append(opt.Customer(0.9, 2000, 10, 3))
customers_0.append(opt.Customer(0.9, 5000, 10, 3))

customers_1 = list()
period = 8
r_demand = (10 - period - 1) * 10.
r_sgm = np.sqrt((10 - period - 1) * np.power(3, 2))
customers_1.append(opt.Customer(0.8, 1000, r_demand, r_sgm))
customers_1.append(opt.Customer(0.9, 2000, r_demand, r_sgm))
customers_1.append(opt.Customer(0.9, 5000, r_demand, r_sgm))

period_1 = framework.period.LastPeriod(customers_1, 39.)
period_0 = framework.period.ApproximatedPeriod(customers_0, 30., period_1)
current_state = opt.StateSpace(demand=[79.23, 76.54, 75.19],
                               filled_demand=[ 0.00, 22.71, 75.19],
                               inventory=9.04)

available_inventory = period_0.supply + current_state.inventory

dim = 50
#  alloc_a = np.linspace(2, 4, dim)
#  alloc_b = np.linspace(0.5, 2, dim)
alloc_a = np.linspace(0, available_inventory, dim)
alloc_b = np.linspace(0, available_inventory, dim)


cost = np.zeros((dim, dim))
d_cost = np.zeros((3, dim, dim))


period_0.approach = 'slsqp'
opt_allocation_h = period_0.get_allocation(current_state)
optimal_cost = period_1.get_cost(period_0.next_state(current_state, opt_allocation_h))

for i_a, x_a in enumerate(alloc_a):
    for i_b, x_b in enumerate(alloc_b):
        this_allocation = [x_a, x_b, available_inventory - x_a - x_b]
        if available_inventory - x_a - x_b < 0:
            cost[i_a, i_b] = np.nan
        else:
            next_state = period_0.next_state(current_state, this_allocation)
            cost[i_a, i_b] = period_1.get_cost(next_state)
            d_cost[:, i_a, i_b] = period_0._d_cost_expected_state(this_allocation, current_state)

# get the optimal allocation
pos = np.nanargmin(cost)
a_opt, b_opt = np.unravel_index(pos, cost.shape)
opt_alloc = [alloc_a[a_opt], alloc_b[b_opt], available_inventory - alloc_a[a_opt] - alloc_b[b_opt]]
optcost_1 = cost.ravel()[pos]
optcost_2 = cost[a_opt, b_opt]
plt.figure(figsize=(6, 5))
plt.pcolormesh(alloc_a, alloc_b, np.ma.array(cost, mask=np.isnan(cost)).T)
plt.plot(opt_alloc[0], opt_alloc[1], 'ro')
plt.plot(opt_allocation_h[0], opt_allocation_h[1], 'gx')
plt.title('Cost by allocation')
plt.xlabel('Allocation to A')
plt.ylabel('Allocation to B')
plt.colorbar()
plt.tight_layout()


for i in range(3):
    plt.figure(figsize=(6, 5))
    plt.pcolormesh(alloc_a, alloc_b, np.ma.array(d_cost[i, :, :], mask=np.isnan(cost)).T)
    plt.plot(opt_alloc[0], opt_alloc[1], 'ro')
    plt.plot(opt_allocation_h[0], opt_allocation_h[1], 'gx')
    plt.title('d cost / dy_{}'.format(i+1))
    plt.xlabel('Allocation to A')
    plt.ylabel('Allocation to B')
    plt.colorbar()
    plt.tight_layout()

print "Gradiant for SLSQP:{}\nGradiant at Optimum: {}".format(period_0._d_cost_expected_state(opt_allocation_h, current_state),
                          period_0._d_cost_expected_state(opt_alloc, current_state)
        )