# -*- coding: utf-8 -*-

import framework.objects as opt
import numpy as np
import matplotlib.pyplot as plt
# import seaborn as sns
import framework.period
import pandas as pd
import time

supply_0 = 10
supply_1 = 120

demand_0 = 10
demand_1= 100

# define customers

sl_a = 0.9
sl_b = 0.8
p_a = 5000
p_b = 100
customers_1 = list()
customers_1.append(opt.Customer(sl_a, p_a, demand_0, 3))
customers_1.append(opt.Customer(sl_b, p_b, demand_0, 3))

customers_2 = list()
customers_2.append(opt.Customer(sl_a, p_a, demand_1, 5))
customers_2.append(opt.Customer(sl_b, p_b, demand_1, 5))



period = [None] * 2
period_1 = framework.period.LastPeriod(customers_2, supply_1)
period_0 = framework.period.ApproximatedPeriod(customers_1, supply_0, period_1)
current_state = opt.StateSpace([0, 0], [0, 0], 0.)
this_allocation = period_0.get_allocation(current_state)
next_allocation = period_1.get_allocation(period_0.next_state(current_state, this_allocation))

allocation_a = np.linspace(0,supply_0,100)
cost = [period_1.get_cost(period_0.next_state(current_state, [t_a, supply_0-t_a])) for t_a in allocation_a]
allocation_next_period = [period_1.get_allocation(period_0.next_state(current_state, [t_a, supply_0-t_a])) for t_a in allocation_a]
d_cost = [period_0._d_cost_expected_state(current_state, [t_a, supply_0 - t_a]) for t_a in allocation_a]
plt.figure()
plt.plot(allocation_a, cost)
plt.plot(this_allocation[0], period_1.get_cost(period_0.next_state(current_state, this_allocation)), 'ro')
plt.title('cost')
plt.figure()
plt.plot(allocation_a, allocation_next_period)
plt.title('allocation in t+1')
plt.figure()
plt.plot(allocation_a, d_cost)
plt.title('d_cost in t')
