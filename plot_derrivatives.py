import matplotlib.pyplot as plt
import framework.objective as ob
from matplotlib import cm
import scipy.stats as stats
import numpy as np


# Define Customer Parameters

distribution = stats.norm(loc=10, scale=2)
penalty = 1000.
sl = 0.9

allocation = np.linspace(0, 15, 50)

demand = 0
filled_demand = 0.8*demand

cost = ob.expected_cost_last_period(allocation, demand, filled_demand, sl, penalty, distribution)
dcost = ob.d_expected_cost_last_period(allocation, demand, filled_demand, sl, penalty, distribution)
ddcost = ob.dd_expected_cost_last_period(allocation, demand, filled_demand, sl, penalty, distribution)

plt.figure()
plt.plot(allocation, cost, label='Cost')
plt.plot(allocation, dcost, label='dCost')
plt.plot(allocation, ddcost, label='ddCost')
plt.legend()
