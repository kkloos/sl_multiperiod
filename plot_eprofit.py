import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import framework.objective as ob
from matplotlib import cm
import scipy.stats as stats
import numpy as np

# Define Customer Parameters

distribution = stats.norm(loc=10, scale=2)
penalty = 1000.
sl = 0.9

allocation = np.linspace(0, 15, 50)
filled_demand = np.linspace(0, 10, 50)
X, Y = np.meshgrid(allocation, filled_demand)

cost = np.zeros((50, 50))
for i in range(50):
    for j in range(50):
        cost[i, j] = ob.expected_cost_last_period(X[i, j], 10, Y[i, j], sl, penalty, distribution)

fig = plt.figure()
ax = fig.gca(projection='3d')
ax.plot_surface(X, Y, cost, cmap=cm.coolwarm, linewidth=0)

demand = np.linspace(0, 100, 50)
X, Y = np.meshgrid(allocation, filled_demand)
cost = np.zeros((50, 50))
for i in range(50):
    for j in range(50):
        cost[i, j] = ob.expected_cost_last_period(X[i, j], Y[i, j], 0.8 * Y[i, j], sl, penalty, distribution)

fig = plt.figure()
ax = fig.gca(projection='3d')
ax.plot_surface(X, Y, cost, cmap=cm.coolwarm, linewidth=0)
