# -*- coding: utf-8 -*-
"""
Created on Wed Sep 20 10:40:30 2017

@author: kok29wv
"""

import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import pandas as pd

df_1 = pd.DataFrame()

df_1['period'] = np.arange(5) + 1
df_1['demand'] = np.ones(5)*10.
df_2 = df_1.copy()
df_1['supply'] = np.arange(12,7,-1)
df_2['supply'] =  list(df_1['supply'][::-1])
df_1['inventory position'] = np.cumsum(df_1['supply']-df_1['demand'])
df_2['inventory position'] = np.cumsum(df_2['supply']-df_2['demand'])


# create plot
colors= sns.color_palette(n_colors=3)
bar_width = 0.2
fig, ax = plt.subplots(2,1)
for plt_id, t_df in enumerate([df_1, df_2]):



    for i, key in enumerate(['demand', 'supply', 'inventory position']):

        rects2 = ax[plt_id].bar(t_df['period'] + i*bar_width, t_df[key], bar_width,
                     color=colors[i],
                     label=key)

    plt.sca(ax[plt_id])
    ax[plt_id].set_xlabel('Period')
    #plt.ylabel('Scores')
    #plt.title('Scores by person')
    plt.xticks(df_1['period'] + 1.5 * bar_width, df_1['period'])

    ax[plt_id].set_xlim(-bar_width+1, 5+3*bar_width)
    plt.ylim(-4,14)

plt.legend(loc='upper center', bbox_to_anchor=(0.5,-0.2), ncol=3)
plt.tight_layout()
plt.subplots_adjust(bottom=0.15)
plt.show()
plt.savefig('plots/ex_period_aggregation.pdf')

eip = sum(df_1['inventory position']), sum(df_2['inventory position'])