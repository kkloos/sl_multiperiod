# -*- coding: utf-8 -*-
"""
Created on Wed Aug 02 08:27:26 2017

@author: kok29wv
"""

import framework.objects as opt
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
#import time as time
# define customers


customers = []
customers.append(opt.Customer(0.9, 1000, 10, 2))
customers.append(opt.Customer(0.9, 1000, 10, 2))

sl_1 = 0.5
sl_2 = 0.9
demand_2 = 10.
demand_1 = np.linspace(0, 100, 25)
# Current Service Level

plt.figure()
ls = ['-', '--', ':']
color = sns.color_palette('muted', n_colors=4)
for i_sl, sl_1 in enumerate([0.5, .9, 1.]):
    for i_supply, supply in enumerate([5., 10., 15., 20.]):
        costs = np.zeros(25)
        allocations = np.zeros(25)

        for i, t_d in enumerate(demand_1):
            ss = opt.StateSpace([t_d, demand_2], [sl_1 * t_d, demand_2 * sl_2], 0.)

            allocation, costs[i] = opt.optimal_allocation_2(supply, ss, customers, method='brentq')
            allocations[i] = allocation[0]
        plt.subplot(211)
        plt.plot(demand_1, allocations / float(supply), label="supply = {}".format(supply), ls=ls[i_sl], color = color[i_supply])
        plt.ylabel('Rel. Allocation Customer 1')
        plt.subplot(212)
        plt.plot(demand_1, costs, label="supply={}, sl={}".format(supply, sl_1), ls=ls[i_sl], color = color[i_supply])
        plt.xlabel('Demand Customer 1')
        plt.ylabel('Total Cost')

plt.subplot(212)
plt.legend(loc=1)
plt.tight_layout()
#plt.savefig("plots/opt_by_demand.pdf")
