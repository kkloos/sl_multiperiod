# -*- coding: utf-8 -*-
"""
Created on Wed Aug 02 08:27:26 2017

@author: kok29wv
"""

import framework.objects as opt
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
# define customers


customers = []
customers.append(opt.Customer(0.9, 1000, 10, 2))
customers.append(opt.Customer(0.9, 1000, 10, 2))



# Current Service Level

plt.figure()
for supply in [10, 15, 20, 25]:
    costs = np.zeros(25)
    allocations = np.zeros(25)
    sl_a = np.linspace(0,1,25)

    for i, sl_1 in enumerate(sl_a):
        ss = opt.StateSpace([10., 10.], [sl_1*10., 9.], 0.)

        allocation, costs[i] = opt.optimal_allocation_2(supply, ss, customers, method = 'brentq')
        allocations[i] = allocation[0]


    plt.subplot(211)
    plt.plot(sl_a, allocations/float(supply), label="supply = {}".format(supply))
    plt.ylabel('Allocation Customer 1')
    plt.subplot(212)
    plt.plot(sl_a, costs)
    plt.xlabel('Service Level Customer 1')
    plt.ylabel('Total Cost')
plt.subplot(211)
plt.legend(loc=3)
plt.tight_layout()
plt.savefig("plots/opt_by_sl.pdf")