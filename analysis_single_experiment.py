from framework.utils.data_interface import Pickle
import framework.analyze as alz
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import numpy as np
import framework.utils.helper as hlp


p = Pickle(2)
demands = p['demands']
allocations = p['allocations']
df_results = p['results']

sl = alz.sl_df(allocations, demands)

plt.figure()
sns.barplot(data=sl, x='Method', y='Value', hue='Customer')
plt.figure()
sns.barplot(data=df_results, x='Method', y='Penalty')

plt.figure()
sns.barplot(data=df_results, x='Method', y='Runtime')

alloc = alz.alloc_df(allocations)

avg_alloc = pd.pivot_table(alloc, values='Value', index=['Period'], columns=['Method', 'Customer'], aggfunc=np.average)
methods = list(avg_alloc.columns.levels[0])
plt.figure()
for t_method in range(len(methods)):
    ax = plt.subplot(len(methods), 1, t_method+1)
    t_df = avg_alloc.loc[:,(methods[t_method], slice(None))]
    t_df.plot(ax = ax)
    
    


avg_alloc2 = pd.pivot_table(alloc, values='Value', index=['Period'], columns=['Method'], aggfunc=np.average)
avg_alloc2.plot()
