# -*- coding: utf-8 -*-
import numpy as np
import pandas as pd
import os.path
from framework.structure import Setup
from framework.utils.data_interface import Pickle

s = Setup(2)
s.t = 10
s.n = 3
s.demand = np.ones((s.n, s.t)) * 10.
s.std = np.ones((s.n, s.t)) * 3.
s.sl = [0.8, 0.9, 0.95]
s.pen = [1000, 2000, 5000]
s.supply = [30] * s.t

p = Pickle(s.id)
p['setup'] = vars(s)
np.random.seed(0)
realizations = 10
demands = np.zeros((s.n, s.t, realizations * 2))
for n in range(s.n):
    for t in range(s.t):
        demands[n, t, :realizations] = np.random.normal(s.demand[n, t], s.std[n, t], realizations)
        demands[n, t, realizations:] = 2. * s.demand[n, t] - demands[n, t, :realizations]
demands = np.maximum(demands, 0)

p['demands'] = demands
p.save()
