import framework.objects as opt
import scipy.integrate as scint
import numpy as np
import scipy.stats as stats
import time

import framework.utils.integration

distri = stats.norm(loc=10, scale=2)

integration = framework.utils.integration.NumericIntegration(distri, 2000)
integration2 = framework.utils.integration.NumericIntegration(distri, 5000)
integration3 = framework.utils.integration.NumericIntegration(distri, 10000)

# probabilty of demand > 10

func = lambda x: np.ones_like(x)

functions = {"Numerical Integration": lambda y: integration.integrate(func, start=y),
             "Numerical Integration w. man interpolation": lambda y: integration.integrate(func, start=y,
                                                                                           mode="interpolate_man"),
             "Numerical Integration w. integration": lambda y: integration3.integrate(func, start=y,
                                                                                     mode="integrate"),
             "Numerical Integration w. n=5000": lambda y: integration2.integrate(func, start=y),
            "Numerical Integration w. n=10000": lambda y: integration3.integrate(func, start=y),
             "Quad": lambda y: scint.quad(distri.pdf, y, np.infty)[0],
             "Accurate": lambda y: 1 - distri.cdf(y)}

range_x = np.linspace(5, 15, 500)

results = {}
times = {}
for this_function in functions.keys():
    start = time.time()
    res = [functions[this_function](x) for x in range_x]
    times[this_function] = time.time() - start
    results[this_function] = np.array(res)
    print "{} took {}s".format(this_function, times[this_function])

import matplotlib.pyplot as plt
plt.figure()
for this_function in functions.keys():
    plt.plot(range_x, results['Accurate'] - results[this_function], label=this_function)

plt.legend()
